
const ajaxRoot = "";
export const docRoot = "/docs";
const authBearer = undefined;
/*
const ajaxRoot = "http://192.168.50.144";
export const docRoot = "http://r2demo.rostell.ru/docs/develop";
const authBearer = "0cc7c12f-0176-6f5f-876b-08002767ddce";
*/
const restRoot = ajaxRoot + "/rest/v1";

export function getDurationStr(seconds_) {
    if (seconds_ === undefined || seconds_ === null)
        return null;
    var minutes = Math.floor(seconds_ / 60);
    var seconds = seconds_ - minutes * 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes - hours * 60;

    var str = ":" + seconds;
    if (str.length < 3)
        str = ":0" + seconds;
    str = minutes + str;
    if (hours)
        str = hours + ":" + str;

    return str;
}

const metaData = {
    productInfo: {
        brandName: "Era platform",
        appName: "Рабочее место администратора"
    },
    dataInfo: {
        restRoot: restRoot,
        ajaxRoot: ajaxRoot,
        authBearer: authBearer
    },
}


export default metaData;