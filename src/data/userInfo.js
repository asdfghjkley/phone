import { kitAjax, processError } from "./data"
import metaData from "./metaData"

function updateUserSettings(params_) {
    var idUser = params_.idUser;
	kitAjax({
		url: metaData.dataInfo.restRoot + metaData.entities.users.data.restPath + "/" + idUser,
		method: "PATCH",
		data: {
			ext: {r21adminData: params_.data}},
        success: function (data_) {
            params_.success();
		},
		error: function (data_) {
            processError(data_, "Ошибка сохранения настроек пользователя", params_.error);
		}
	});
}

class userInfo {
    login = "login";
    domain = "domain";
    sessionInfo = {};
    userSettings = {
        mode: "base",
        theme: "light"
    };

    contentRef = null;

    setUserSetting(name_, value_) {
        var that = this;

        var data = {...that.userSettings}
        data[name_] = value_;

        updateUserSettings({
            idUser: this.sessionInfo.user_id,
            data: data,
            success: function () {
                that.userSettings[name_] = value_;
                var state = that.contentRef.state;
                state[name_] = value_;
                if (that.contentRef)
                    that.contentRef.setState(state);
            },
        })
    }

    setMode(mode_) {
        this.setUserSetting("mode", mode_);
    }

    setTheme(theme_) {
        this.setUserSetting("theme", theme_);
    }
}

var instance = new userInfo();

export default instance;