import $ from 'jquery';
import metaData from './metaData';
import notify from "devextreme/ui/notify";

export function kitAjax(params_) {
	var contentType = params_.contentType;

	var data = null;
	if (params_.data) {
		data = JSON.stringify(params_.data);
		contentType = contentType ?? "application/json; charset=UTF-8";
	}

	var headers = {};
	if (metaData.dataInfo.authBearer) {
		headers.Authorization = "Bearer " + metaData.dataInfo.authBearer;
	}

	$.ajax(params_.url, {
		method: params_.method,
		contentType: contentType,
		headers: headers,
		data: data,
		success: function (data_, status_, xhr_) {
			if (params_.success)
				params_.success(data_, status_, xhr_);
		},
		error: function (response_) {
			processError(response_, "Ошибка", params_.error);
		}
	});
}

export function processError(data_, defaultMsg_, reject_) {
	var msg = defaultMsg_;
	if (data_.statusText)
		msg = data_.statusText;
	if (data_.responseJSON && data_.responseJSON.error_message)
		msg = data_.responseJSON.error_message;

	if (reject_)
		reject_(msg);

	notify(msg, "error", 3000);
}

