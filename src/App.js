import 'devextreme/dist/css/dx.common.css';
import './themes/generated/theme.base.css';
import './themes/generated/theme.light.css';
import './themes/generated/theme.dark.css';
import './themes/generated/theme.light_compact.css';
import './themes/generated/theme.dark_compact.css';
import React, { useState } from 'react';
import './dx-styles.scss';
import { useScreenSize, useScreenSizeClass } from './utils/media-query';
import MainComponent from './components/Main';
import { Storage } from "./core/engine/Storage"
import { GlobalUtils } from './utils/GlobalUtils';

function Render() {
    const screenSizeClass = useScreenSizeClass();
    const screenSize = useScreenSize();
    
    const [theme, setTheme] = useState(Storage.Instance.configuration.design.theme);
    Storage.Instance.subscribeToConfiguration(() => {
        GlobalUtils.logDebug("App: configuration changed detected");
        setTheme(Storage.Instance.configuration.design.theme);
    });

    var compact = "";
    if (screenSizeClass.indexOf("small") >= 0)
        compact = "_compact";
    
    GlobalUtils.logDebug("App.render: size=" + screenSizeClass + " portrait=" + screenSize.isPortrait + " theme=" + theme + " compact=" + compact);

    return (
        <div className={`${screenSizeClass} dx-swatch-${theme}${compact}`}>
            <MainComponent />
        </div>
    );
}

export default Render;