export enum ECallState {
    Unknown = "Unknown",
    Dialing = "Dialing",
    Ringing = "Ringing",
    Established = "Established",
    Ghost = "Ghost",
    Terminating = "Terminating",
    Terminated = "Terminated"
}

export enum ECallDirection {
    Unknown = "Unknown",
    In = "In",
    Out = "Out"
}

export class ICall {
    dispose() { }
    
    make(number_: string, name_: string) { }
    changed() {
    }

    canAnswer() { return false; }
    answer() { }

    canDrop() { return false; }
    drop() { }

    canHold() { return false; }
    hold() { }

    canUnHold() { return false; }
    unhold() { }
    
    canTransferTo() { return false; }
    transferTo(number_: string) { }

    canConfirmTransfer() { return false; }
    confirmTransfer() { }

    canMute() { return false; }
    mute() { }

    canUnMute() { return false; }
    unmute() { }

    canSendDTMF() { return false; }
    sendDTMF(digit_: string) { }
    
    _nativeID: string = "";
    _reason: string = "";
    get reason(): string { return this._reason; }

    _id: string = "";
    get id(): string { return this._id; }

    _timestamp: number = 0;
    get timestamp(): number { return this._timestamp; }

    _number: string = "";
    get number(): string { return this._number; }
    set number(value_: string) { this._number = value_; this.changed(); }

    _name: string = "";
    get name(): string { return this._name; }
    set name(value_: string) { this._name = value_; this.changed(); }

    _state: ECallState = ECallState.Unknown;
    get state(): ECallState { return this._state; }
    set state(value_: ECallState) { this.setState(value_); }
    setState(state_: ECallState) { }

    _held: boolean = false;
    get held(): boolean { return this._held; }
    set held(value_: boolean) { this._held = value_; this.changed(); }

    _muted: boolean = false;
    get muted(): boolean { return this._muted; }
    set muted(value_: boolean) { this._muted = value_; this.changed(); }

    _direction: ECallDirection = ECallDirection.Unknown;
    get direction(): ECallDirection { return this._direction; }

    _idaccount: string = "";
    get idaccount() { return this._idaccount; }

    _hasVideo: boolean = false;
    get hasVideo() {
        return this._hasVideo;
    }

}