import { IAccount } from "./account";
import { ICall } from "./call";

export type IManagerDelegates = {
    accountsChanged: () => void;
    accountChanged: (account_: IAccount) => void;
    callCreated: (account_: IAccount, call_: ICall) => void;
    callDeleted: (account_: IAccount, call_: ICall) => void;
    callChanged: (account_: IAccount, call_: ICall) => void;
}
