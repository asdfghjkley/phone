import { GlobalUtils } from "../../utils/GlobalUtils";
import { IAccountConfiguration } from "../config/account";
import { ICall } from "./call";

export enum EAccountState {
    Initializing = "Initializing",
    Ready = "Ready",
    Error = "Error",
    Disabled = "Disabled"
}

var accountCounter: number = 1;

export class IAccount {
    constructor(config_: IAccountConfiguration, delegates_: IAccountDelegates) {
        this._internalCounter = (accountCounter++);
        GlobalUtils.logDebug("IAccount#" + this._internalCounter + ".create");
        this._config = GlobalUtils.clone(config_);
        this._delegates = delegates_;
    }
    init() {
        this.configure(this._config);
    }
    dispose() { 
        GlobalUtils.logDebug("IAccount#" + this._internalCounter + ".dispose")
    }
    changed() { }
    configure(config_: IAccountConfiguration) { }
    applyConfig() { }

    makeCall(number_: string, name_: string) { }

    callCreated(call_: ICall) { }
    callDeleted(call_: ICall) { }
    callChanged(call_: ICall) { }

    _internalCounter: number;

    get id(): string { return this._config.id; }
    get name(): string { return this._config.name; }

    get config(): IAccountConfiguration { return this._config; }
    _config: IAccountConfiguration;

    get state(): EAccountState { return this._state; }
    set state(value_: EAccountState) { this._state = value_; this.changed(); }
    _state: EAccountState = EAccountState.Error;

    get errorMessage(): string { return this._errorMessage; }
    _errorMessage: string = "Not initialized";

    _calls: ICall[] = [];
    get calls(): ICall[] { return this._calls;}

    _delegates: IAccountDelegates;
}

export type IAccountDelegates = {
    accountChanged: (account_: IAccount) => void;
    callCreated: (account_: IAccount, call_: ICall) => void;
    callDeleted: (account_: IAccount, call_: ICall) => void;
    callChanged: (account_: IAccount, call_: ICall) => void;
}