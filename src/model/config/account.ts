import { GlobalUtils } from "../../utils/GlobalUtils";

export enum EEngineType {
    Unknown = "Unknown",
    SipJS = "SipJS",
    Demo = "Demo"
}

export class IAccountConfiguration {
    constructor() {
        this.id = GlobalUtils.genUuid();    
    }

    id: string;
    name: string = "";
    enabled: boolean = true;
    engineType: EEngineType = EEngineType.Unknown;
    userName: string = "";
    domainName: string = "";
    login: string = "";
    password: string = "";
    serverAddress: string = "";
    serverPort: number = 5063;
    register: boolean = true;
    video: boolean = false;
}