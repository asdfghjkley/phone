import { IAccountConfiguration } from "./account";

class Design {
    showDialpad: boolean = true;
    showHistory: boolean = true;
    theme: string = "dark";
}

class Options {
    logLevel: string = "error";
    displayName: string = "";

    autoAnswer = false;
    autoAnswerTimeout = 5;
    historySize = 10;

    codecsAudio: string = "";
    codecsVideo: string = "";
    codecsVideoDisableRTX: boolean = false;
    iceTimeout: number = 1000;

    registrationExpires: number = 180;

    allowMultipleActiveCalls: boolean = false;
    incomingCallLimit: number = 0;
    ringSound: string = "ring";
}

class IConfiguration {
    version = "1.0.0";

    accounts: IAccountConfiguration[] = [];
    design = new Design();    
    options = new Options();
}

export { IConfiguration }