import { IConfiguration } from "../config/main"

export type SettingsDelegates = {
    cancel: () => void;
    save: (settings_: IConfiguration) => void;
}

export type SettingsData = {
    visible: boolean;
    settings: IConfiguration;
}

export type SettingsProps = {
    data: SettingsData;
    delegates: SettingsDelegates;
}
  