import { ECallState, ECallDirection } from "../core/call"

export type ICurrentCall = {
    id: string;
    timestamp: number;
    duration: number;
    stateTime: number;
    deleteTime: number;
    number: string;
    name: string;
    state: ECallState;
    reason: string;
    direction: ECallDirection;
    idaccount: string;

    hasVideo: boolean;

    canAnswer: boolean;
    canDrop: boolean;
    canHold: boolean;
    canUnhold: boolean;
    canMute: boolean;
    canUnmute: boolean;
    canTransferTo: boolean;
    canConfirmTransfer: boolean;

    transferNumber: string;
}

export type ICurrentCallsData = {
    list: ICurrentCall[];
}

export type ICurrentCallsDelegates = {
    answer: (call_: ICurrentCall) => void;
    drop: (call_: ICurrentCall) => void;
    hold: (call_: ICurrentCall) => void;
    unhold: (call_: ICurrentCall) => void;
    transferTo: (call_: ICurrentCall) => void;
    confirmTransfer: (call_: ICurrentCall) => void;
    mute: (call_: ICurrentCall) => void;
    unmute: (call_: ICurrentCall) => void;
}

export type CurrentCallsProps  = {
    data: ICurrentCallsData,
    delegates: ICurrentCallsDelegates
}

export type ICurrentCallData = ICurrentCall;

export type ICurrentCallDelegates = ICurrentCallsDelegates;

export type CurrentCallProps  = {
    data: ICurrentCallData,
    delegates: ICurrentCallDelegates
}
  