import { ECallDirection } from "../core/call";

export type IHistoryCall = {
    id: string;
    timestamp: number;
    number: string;
    name: string;
    direction: ECallDirection;
    duration: number;
    idaccount: string;
}

export type IHistoryCallsData = {
    list: IHistoryCall[];
}

export type IHistoryCallsDelegates = {
    callDblClick: (call_: IHistoryCall) => void;
}

export type CallsHistoryProps = {
    data: IHistoryCallsData,
    delegates: IHistoryCallsDelegates,
    isOpen: boolean,
    toggleOpen: (shouldOpen: boolean) => void,
}

export type IHistoryCallData = IHistoryCall;

export type IHistoryCallDelegates = IHistoryCallsDelegates;

export type CallHistoryProps  = {
    data: IHistoryCallData,
    delegates: IHistoryCallDelegates,
}
  