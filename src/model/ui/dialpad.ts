export type IDialpadData = {
}

export type IDialpadDelegates = {
    digitClick: (digit_: string) => void;
}

export type DialpadProps = {
    data: IDialpadData,
    delegates: IDialpadDelegates
}
  