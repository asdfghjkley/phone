export type IDestinationData = {
    number: string;
}

export type IDestinationDelegates = {
    numberChanged: (number_: string) => void;
    yesClick: () => void;
    noClick: () => void;
}

export type DestinationProps = {
    data: IDestinationData,
    delegates: IDestinationDelegates
}
  