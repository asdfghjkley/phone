import { EAccountState } from "../core/account"

export type IAccount = {
    id: string;
    name: string;
    state: EAccountState;
    current: boolean;
}

export type IAccountsData = {
    list: IAccount[];
}

export type IAccountsDelegates = {
    accountClick: (account_: IAccount) => void;
    settingsClick: () => void;
}

export type AccountsProps = {
    data: IAccountsData,
    delegates: IAccountsDelegates
}
  