import { IConfiguration } from "../config/main"
import { IAccountsData, IAccountsDelegates } from "./accounts"
import { IHistoryCallsData, IHistoryCallsDelegates } from "./callshistory"
import { ICurrentCallsData, ICurrentCallsDelegates } from "./currentcalls"
import { IDestinationData, IDestinationDelegates } from "./destination"
import { IDialpadData, IDialpadDelegates } from "./dialpad"

export type ISoftphoneDelegates = {
    accounts: IAccountsDelegates;
    destination: IDestinationDelegates;
    dialpad: IDialpadDelegates;
    history: IHistoryCallsDelegates;
    calls: ICurrentCallsDelegates;
    start: () => void,
}

export type ISoftphoneData = {
    accounts: IAccountsData;
    destination: IDestinationData;
    dialpad: IDialpadData;
    history: IHistoryCallsData;
    calls: ICurrentCallsData;
    configuration: IConfiguration;
}

export type SoftphoneProps = {
    data: ISoftphoneData,
    delegates: ISoftphoneDelegates,
}
  