import { GlobalUtils } from "./GlobalUtils";

export interface IVideo {
    id: string;
    cookie?: string;
    local: HTMLVideoElement;
    remote: HTMLVideoElement;
    busy: boolean;
}

export class VideoUtils {
    private static _videos: IVideo[] = [];

    private static counter = 0;
    static getVideos(): IVideo {
        var cookie = GlobalUtils.genUuid();

        var result = this._videos.find(item_ => !item_.busy);
        if (result) {
            GlobalUtils.logDebug("VideoUtils.getVideos: found videos id=" + result.id + " for cookie=" + cookie);
            result.busy = true;
            result.cookie = cookie;
            return result;
        }

        var id = GlobalUtils.genUuid();

        var remoteVideo = document.createElement("video");
        remoteVideo.id = "remoteVideo_" + id;
        remoteVideo.className = "flex video";
        remoteVideo.setAttribute("poster", process.env.PUBLIC_URL + "/img/user.png");

        var localVideo = document.createElement("video");
        localVideo.id = "localVideo_" + id;
        localVideo.className = "flex video";
        localVideo.setAttribute("poster", process.env.PUBLIC_URL + "/img/user.png");
        // localVideo.setAttribute("muted", "");

        GlobalUtils.logDebug("VideoUtils.getVideos: created videos id=" + id + " for cookie=" + cookie + " counter=" + (++this.counter));
        result = { id, cookie, remote: remoteVideo, local: localVideo, busy: true };
        this._videos.push(result);
        return result;
    }

    static freeVideos(cookie_: string) {
        GlobalUtils.logDebug("VideoUtils.freeVideos: cookie=" + cookie_);
        var result = this._videos.find(item_ => item_.busy && item_.cookie === cookie_);
        if (result) {
            if (result.busy && result.cookie) {
                result.remote.pause();
                result.remote.removeAttribute("src");
                result.remote.srcObject = null;
                result.local.pause();
                result.local.removeAttribute("src");
                result.local.srcObject = null;
                result.cookie = undefined;
                result.busy = false;
            }
        }
    }
}