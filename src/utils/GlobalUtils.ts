import notify from "devextreme/ui/notify";
import { Storage } from "../core/engine/Storage";

export class GlobalUtils {
    static _garbageCounter: number = 0;
    static forceGarbageCollector() {
        // force garbage collection every 10 calls to prevent RTCPeerConnection leaks

        if (this._garbageCounter++ % 10 === 0) {
            this.logDebug("GlobalUtils.forceGarbageCollector started");
            queueMicrotask(() => {
                let img: any = document.createElement("img");
                img.src = window.URL.createObjectURL(new Blob([new ArrayBuffer(5e+7)])); // 50Mo or less or more depending as you wish to force/invoke GC cycle run
                img.onerror = function () {
                    window.URL.revokeObjectURL(this.src);
                    img = null
                }
                this.logDebug("GlobalUtils.forceGarbageCollector finished");
            });
        }
    }

    static logDebug(message_: string) {
        if (Storage.Instance.configuration.options.logLevel === "debug")
            console.log(new Date().toLocaleString()+": " + message_);
    }

    static logError(message_: string) {
        console.error(new Date().toLocaleString()+": " + message_);
    }

    static isNullOrEmpty(object_: any) {
        return object_ === undefined || object_ === null || object_ === "";
    }

    static genUuid(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : ((r & 0x3) | 0x8);
            return v.toString(16);
        });
    }

    static notifyError(message_: string) {
        notify(message_, "error", 3000)
    }

    static notifyWarning(message_: string) {
        notify(message_, "warning", 3000)
    }

    static dateToStrTime(timestamp_: number): string {
        var timestamp = new Date(timestamp_);
        var timestampTime = timestamp.toLocaleTimeString();
    
        var seconds = timestamp.getSeconds().toString();
        if (seconds.length < 2)
            seconds = "0" + seconds;
        seconds = ":" + seconds + " ";
    
        return timestampTime.replace(seconds, " ");
    }
    
    static dateToStrDate(timestamp_: number): string {
        var timestamp = new Date(timestamp_);
        var today = new Date();
    
        var timestampDate = timestamp.toLocaleDateString();
        var todayDate = today.toLocaleDateString();
    
        if (todayDate === timestampDate)
            return "";
        else
            return timestampDate;
    }
    
    static getDurationStr(seconds_: number) {
        if (seconds_ === undefined || seconds_ === null)
            return null;
        var minutes = Math.floor(seconds_ / 60);
        var seconds = seconds_ - minutes * 60;
        var hours = Math.floor(minutes / 60);
        minutes = minutes - hours * 60;
    
        var str = ":" + seconds;
        if (str.length < 3)
            str = ":0" + seconds;
        str = minutes + str;
        if (hours) {
            if (str.length < 5)
                str = "0" + str;
            str = hours + ":" + str;
        }
    
        return str;
    }
    
    static clone(source_: any): any {
        if (source_)
            return JSON.parse(JSON.stringify(source_));
        else
            return source_;
    }

    static getUrlParams(url_: string) {
        var request: any = {};
        if (url_.indexOf('?') <= 0)
            return request;
        var pairs = url_.substring(url_.indexOf('?') + 1).split('&');
        for (var i = 0; i < pairs.length; i++) {
            if (!pairs[i])
                continue;
            var pair = pairs[i].split('=');
            request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }
        return request;
    }
}