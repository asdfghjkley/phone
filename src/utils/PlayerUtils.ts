import { GlobalUtils } from "./GlobalUtils";

interface IPlayer {
    id: string;
    cookie?: string;
    player: HTMLAudioElement;
    div: HTMLDivElement;
    busy: boolean;
}

export class PlayerUtils {

    private static _players: IPlayer[] = [];
    private static _busyPlayers: { [cookie: string]: IPlayer } = {};

    private static counter = 0;
    private static getPlayer(cookie_: string): IPlayer {
        var result = this._players.find(item_ => !item_.busy);
        if (result) {
            result.busy = true;
            result.cookie = cookie_;
            this._busyPlayers[cookie_] = result;
            GlobalUtils.logDebug("PlayerUtils.getPlayer: found player id=" + result.id + " for cookie="+cookie_);
            return result;
        }

        var player = document.createElement("audio");
        var div = document.createElement("div");
        div.appendChild(player);
        document.body.appendChild(div);
        div.style.display = "none";
        var id = GlobalUtils.genUuid();

        GlobalUtils.logDebug("PlayerUtils.getPlayer: created player id=" + id + " for cookie="+cookie_ + " counter=" + (++this.counter));
        result = { id, cookie: cookie_, player, div, busy: true };
        this._players.push(result);
        this._busyPlayers[cookie_] = result;

        player.onended = () => {
            if (result && result.cookie) {
                delete this._busyPlayers[result.cookie];
                result.cookie = undefined;
                result.busy = false;
            }
        }

        return result;
    }

    static _lastBusyTime?: number;
    static playStart(sound_: string, loop_: boolean): string | undefined {
        GlobalUtils.logDebug("PlayerUtils.playStart: " + sound_);

        if (sound_ === "busy") {
            if (new Date().getTime() - (this._lastBusyTime ?? 0) < 2000) {
                GlobalUtils.logDebug("PlayerUtils.playStart: second busy ignored");
                return;
            }
            this._lastBusyTime = new Date().getTime();
        }

        var cookie = GlobalUtils.genUuid();
        var player = this.getPlayer(cookie);
        if (player && player.player) {
            player.player.pause();
            player.player.removeAttribute("src");
            player.player.srcObject = null;
            player.player.src = process.env.PUBLIC_URL + "/sound/" + sound_ + ".mp3";
            player.player.loop = loop_;
            player.player.load();

            player.player.play().catch(reason_ => {
                GlobalUtils.logError("PlayerUtils.playStart.play failed");
                GlobalUtils.logError(reason_);
            });
            return cookie;
        }
        else
            return undefined;
    }

    static playStop(cookie_: string) {
        GlobalUtils.logDebug("PlayerUtils.playStop cookie=" + cookie_);
        var player = this._busyPlayers[cookie_];
        if (player) {
            if (player.busy && player.cookie) {
                GlobalUtils.logDebug("PlayerUtils.playStop player found and cleaned");
                if (player.player) {
                    player.player.pause();
                    player.player.removeAttribute("src");
                    player.player.srcObject = null;
                }
                delete this._busyPlayers[player.cookie];
                player.cookie = undefined;
                player.busy = false;
            }
        }
    }
}