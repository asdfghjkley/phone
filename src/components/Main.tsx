import React from 'react';
//import ruMessages from "devextreme/localization/messages/ru.json";
//import { locale, loadMessages } from "devextreme/localization";
import { Storage } from '../core/engine/Storage';
import Softphone from './softphone/Softphone';
import SettingsPopup from './settings/SettingsPopup';
import { AccountManager } from '../core/engine/AccountManager';
import { ISoftphoneData, ISoftphoneDelegates } from '../model/ui/softphone';
import { IHistoryCall } from '../model/ui/callshistory';
import { GlobalUtils } from '../utils/GlobalUtils';
import * as uiAccounts from '../model/ui/accounts';
import * as coreAccounts from '../model/core/account';
import { ECallDirection, ECallState, ICall } from '../model/core/call';
import { ICurrentCall } from '../model/ui/currentcalls';
import { SettingsData, SettingsDelegates } from '../model/ui/settings';
import { IConfiguration } from '../model/config/main';
import { PlayerUtils } from '../utils/PlayerUtils';

type MainProps = {
}

type MainState = {
    ui: {
        data: ISoftphoneData,
        delegates: ISoftphoneDelegates
    },
    settings: {
        data: SettingsData,
        delegates: SettingsDelegates
    },
    started: boolean
}
/*
var i = 1;


/*
function peer() {
  var peer: any = new RTCPeerConnection();
  setTimeout(() => {
    peer.close();
    peer = undefined;
  }, 10);
    console.log(i++);
  if (!(i % 100)) {
      forceGarbageCollector();
  }
}
*/

class MainComponent extends React.Component<MainProps, MainState> {
    debugTimer: any;

    constructor(props: MainProps) {
        super(props);
        var that = this;

        GlobalUtils.logDebug("@ Softphone started. Version=1.2.0/2022.02.12");

        Storage.Instance.load();

        var data: ISoftphoneData = {
            accounts: {
                list: []
            },
            destination: {
                number: ""
            },
            dialpad: {},
            history: {
                list: GlobalUtils.clone(Storage.Instance.history)
            },
            calls: {
                list: []
            },
            configuration: GlobalUtils.clone(Storage.Instance.configuration)
        };

        var delegates = {
            start: () => this.start(),
            accounts: {
                accountClick: function (account_: uiAccounts.IAccount) {
                    var account = that._manager?.accounts.find(item_ => item_.id === account_.id);
                    if (account) {
                        switch (account.state) {
                            case coreAccounts.EAccountState.Error:
                                GlobalUtils.notifyError(account.errorMessage);
                                break;
                            case coreAccounts.EAccountState.Disabled:
                                GlobalUtils.notifyWarning("Account is disabled");
                                break;
                            case coreAccounts.EAccountState.Initializing:
                                GlobalUtils.notifyWarning("Account is initializing");
                                break;
                            case coreAccounts.EAccountState.Ready:
                                that.state.ui.data.accounts.list.forEach(item_ => {
                                    item_.current = item_.id === account_.id;
                                })
                                that.updateState();
            
                                break;
                        }
                    }
                    else {
                        GlobalUtils.notifyError("Account not found");
                    }
                },
                settingsClick: function () {
                    var state = that.state;
                    state.settings.data = {
                        settings: GlobalUtils.clone(Storage.Instance.configuration),
                        visible: true
                    };
                    that.setState(that.state);
                }
            },
            destination: {
                yesClick: () => {
                    var data: ISoftphoneData = that.state.ui.data;

                    var number = data.destination.number;
                    if (number) {
                        this.setDestinationNumber("", false);
            
                        var idAccount = that.getCurrentAccountID();
                        if (idAccount)
                            this._manager?.makeCall(idAccount.toString(), number.trim(), "");
                        else
                            GlobalUtils.notifyError("Have no accounts to call");
                    }
                    else {
                        var call = data.calls.list.find(item => item.state === ECallState.Ringing);
                        if (call) {
                            this._manager?.answerCall(call.id);
                            return;
                        }

                        var historyCall = data.history.list.find(item => item.direction === ECallDirection.Out);
                        if (historyCall)
                            this.setDestinationNumber(historyCall.number, false);
                    }

                    that.updateState();
                },
                noClick: () => {
                    var data: ISoftphoneData = that.state.ui.data;
                    if (data.destination.number) {
                        this.setDestinationNumber("", true);
                    }
                    else {
                        that._manager?.dropAllCalls();
                    }
                },
                numberChanged: (number_: string) => {
                    this.setDestinationNumber(number_, true);
                }
            },
            dialpad: {
                digitClick: (digit_: string) => {
                    if (!this._manager?.sendDTMFtoActiveCalls(digit_)) {
                        var data: ISoftphoneData = that.state.ui.data;
                        this.setDestinationNumber(data.destination.number + digit_, true);
                    }
                }
            },
            history: {
                callDblClick: (call_: IHistoryCall) => {
                    var data: ISoftphoneData = that.state.ui.data;
                    var idPreferredAccount = call_.idaccount;

                    var preferredAccount = data.accounts.list.find(item => item.id === idPreferredAccount && item.state === coreAccounts.EAccountState.Ready);
            
                    if (preferredAccount)
                        that._manager?.makeCall(call_.idaccount, call_.number.trim(), call_.name);
                    else {
                        var idAccount = that.getCurrentAccountID();
                        if (idAccount)
                            that._manager?.makeCall(idAccount.toString(), call_.number.trim(), call_.name);
                        else
                            GlobalUtils.notifyError("Have no accounts to call");
                    }
                }
            },
            calls: {
                answer: (call_: ICurrentCall) => {
                    GlobalUtils.logDebug("Main.answer sphdebug A");
                    that._manager?.answerCall(call_.id);
                    GlobalUtils.logDebug("Main.answer sphdebug B");
                },
                drop: (call_: ICurrentCall) => {
                    that._manager?.dropCall(call_.id);
                },
                hold: (call_: ICurrentCall) => {
                    that._manager?.holdCall(call_.id);
                },
                unhold: (call_: ICurrentCall) => {
                    that._manager?.unholdCall(call_.id);
                },
                transferTo: (call_: ICurrentCall) => {
                    var number = that.state.ui.data.destination.number;
                    if (GlobalUtils.isNullOrEmpty(number)) {
                        GlobalUtils.notifyWarning("Number is empty");
                        return;
                    }
                    that._manager?.transferCallTo(call_.id, number);
                    this.setDestinationNumber("", true);
                },
                confirmTransfer: (call_: ICurrentCall) => {
                    that._manager?.confirmTransfer(call_.id);
                },
                mute: (call_: ICurrentCall) => {
                    that._manager?.muteCall(call_.id);
                },
                unmute: (call_: ICurrentCall) => {
                    that._manager?.unmuteCall(call_.id);
                }
            }
        };

        this.state = {
            ui: {
                data,
                delegates,
            },
            settings: {
                data: {
                    visible: false,
                    settings: GlobalUtils.clone(Storage.Instance.configuration)
                },
                delegates: {
                    cancel: that.settingsDialogClosed,
                    save: that.settingsDialogSave
                }
            },
            started: false
        }
    }
  
    _manager: AccountManager | null = null;
    _timer: NodeJS.Timeout | null = null;

    setDestinationNumber(number_: string, updateState_: boolean) {
        var that = this;
        var data: ISoftphoneData = that.state.ui.data;
        data.destination.number = number_;
        data.calls.list.forEach(item_ => item_.transferNumber = number_);
        if (updateState_)
            that.updateState();
    }

    componentDidMount() {
        this._manager = new AccountManager({
            accountsChanged: this.accountsChanged,
            accountChanged: this.accountChanged,
            callCreated: this.callCreated,
            callChanged: this.callChanged,
            callDeleted: this.callDeleted,
        });

//        this._manager.applyConfig(Storage.Instance.configuration);

        this._timer = setInterval(this.onTimer.bind(this), 1000);
    }

    componentWillUnmount() {
        if (this._timer) {
            clearInterval(this._timer);
            this._timer = null;
        }

        if (this._manager) {
            this._manager.dispose();
            this._manager = null;
        }
    }

    onTimer() {
        var calls = this.state.ui.data.calls;
        var now = new Date().getTime();
        var liveCalls: ICurrentCall[] = [];
        calls.list.forEach(call_ => {
            if (call_.state === ECallState.Terminated) {
                if (call_.deleteTime > 0 && call_.deleteTime + 5000 < now) {
                }
                else
                    liveCalls.push(call_);
            }
            else
                liveCalls.push(call_);
        });

        if (liveCalls.length !== calls.list.length)
            calls.list = liveCalls;

        this.updateState();
    }

    getCurrentAccountID = (): string | null => {
        var that = this;
        var data: ISoftphoneData = that.state.ui.data;

        var current = data.accounts.list.find(item => item.current && item.state === coreAccounts.EAccountState.Ready);
        if (current)
            return current.id;

        var first = data.accounts.list.find(item => item.state === coreAccounts.EAccountState.Ready);
        if (first)
            return first.id;
  
        return null;
    }

    accountsChanged = () => {
        var that = this;
        var data: ISoftphoneData = that.state.ui.data;

        // todo: smart account update instead of full replace
        data.accounts.list = [];

        this._manager?.accounts.forEach(account_ => {
            var account: uiAccounts.IAccount = {
                id: account_.id,
                name: account_.name,
                state: account_.state,
                current: false
            };
            data.accounts.list.push(account);
        });
    
        that.updateState();
    }

    accountChanged = (account_: coreAccounts.IAccount) => {
        var uiAccount: uiAccounts.IAccount | undefined = this.state.ui.data.accounts.list.find(item_ => item_.id === account_.id);
        if (uiAccount) {
            uiAccount.state = account_.state;

            this.updateState();
        }
    }

    createCall(): ICurrentCall {
        return {
            id: "",
            timestamp: 0,
            stateTime: 0,
            deleteTime: 0,
            duration: -1,
            number: "",
            name: "",
            state: ECallState.Unknown,
            reason: "",
            direction: ECallDirection.Unknown,
            idaccount: "",
            hasVideo: false,
            canAnswer: false,
            canDrop: false,
            canHold: false,
            canUnhold: false,
            canMute: false,
            canUnmute: false,
            canTransferTo: false,
            transferNumber: this.state.ui.data.destination.number,
            canConfirmTransfer: false
        }
    }

    updateCall = (uiCall_: ICurrentCall, call_: ICall) => {
        if (call_.state === ECallState.Terminated || call_.state === ECallState.Terminating) {
            if (uiCall_.state === ECallState.Established)
                uiCall_.duration = Math.round((new Date().getTime() - uiCall_.stateTime) / 1000);
            else if (uiCall_.state !== ECallState.Terminated && uiCall_.state !== ECallState.Terminating)
                uiCall_.duration = 0;
        }

        if (uiCall_.state !== call_.state)
            uiCall_.stateTime = new Date().getTime();

        uiCall_.id = call_.id;
        uiCall_.timestamp = call_.timestamp;
        uiCall_.number = call_.number;
        uiCall_.name = call_.name;
        uiCall_.state = call_.state;
        uiCall_.reason = call_.reason;
        uiCall_.direction = call_.direction;
        uiCall_.idaccount = call_.idaccount;

        uiCall_.hasVideo = call_.hasVideo;

        uiCall_.canAnswer = call_.canAnswer();
        uiCall_.canDrop = call_.canDrop();
        uiCall_.canHold = call_.canHold();
        uiCall_.canUnhold = call_.canUnHold();
        uiCall_.canMute = call_.canMute();
        uiCall_.canUnmute = call_.canUnMute();
        uiCall_.canTransferTo = call_.canTransferTo();
        uiCall_.canConfirmTransfer = call_.canConfirmTransfer();
        uiCall_.transferNumber = this.state.ui.data.destination.number;
    }

    callCreated = (account_: coreAccounts.IAccount, call_: ICall) => {
        var that = this;
        var data = that.state.ui.data;

        var call = this.createCall();
        this.updateCall(call, call_)
        data.calls.list.splice(0, 0, call);
    
        that.updateState();
    }

    callChanged = (account_: coreAccounts.IAccount, call_: ICall) => {
        var that = this;
        var data = that.state.ui.data;
      
        var call = data.calls.list.find(item_ => item_.id === call_.id);
        if (call) {
            var index = data.calls.list.indexOf(call);
            if (index >= 0) {
                this.updateCall(call, call_);
                that.updateState();
            }
        }
    }

    callDeleted = (account_: coreAccounts.IAccount, call_: ICall) => {
        var that = this;
        var data = that.state.ui.data;
      
        var call = data.calls.list.find(item_ => item_.id === call_.id);
        if (call) {
            call.state = ECallState.Terminated;
            call.deleteTime = new Date().getTime();

            data.history.list.splice(0, 0, {
                id: call.id,
                timestamp: call.timestamp,
                number: call.number,
                name: call.name,
                duration: call.duration,
                direction: call.direction,
                idaccount: call.idaccount
            });

            var maxCalls = Storage.Instance.configuration.options.historySize;
            if (data.history.list.length > maxCalls)
                data.history.list.splice(maxCalls, data.history.list.length - maxCalls);
            
            Storage.Instance.history = GlobalUtils.clone(data.history.list);
            Storage.Instance.save();

            that.updateState();
        }
    }

    updateState = () => {
        if (this.state.settings.data.visible)
            return;
        this.setState(this.state);
    }

    settingsDialogClosed = () => {
        var state = this.state;
        state.settings.data.visible = false;
        state.ui.data.configuration = GlobalUtils.clone(Storage.Instance.configuration);
        this.setState(this.state);
    }

    settingsDialogSave = (settings_: IConfiguration) => {
        try {
            Storage.Instance.configuration = GlobalUtils.clone(settings_);
            Storage.Instance.save();
            this._manager?.applyConfig(Storage.Instance.configuration);
            this.settingsDialogClosed();
        }
        catch (exception: any) {
            GlobalUtils.notifyError(exception.message);
        }
    }

    start = () => {
        this.setState({ ...this.state, started: true });
        PlayerUtils.playStart("alert", false);
        this._manager?.applyConfig(Storage.Instance.configuration);
    }

    render = () => {
        return <>
            <Softphone
                data={this.state.ui.data}
                delegates={this.state.ui.delegates}
            />
            {this.state.settings.data.visible &&
                <SettingsPopup
                    data={this.state.settings.data}
                    delegates={this.state.settings.delegates}
                />
            }
        </>
    }
}

export default MainComponent;
