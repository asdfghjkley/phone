import '../ui.css';
import React from 'react';
import * as ui from '../../model/ui/settings';
import { Form, Popup, ScrollView, TabPanel } from 'devextreme-react';
import { getScreenSize } from '../../utils/media-query';
import { SimpleItem, GroupItem, ButtonItem } from 'devextreme-react/form';
import { ToolbarItem } from 'devextreme-react/popup';
import { IConfiguration } from '../../model/config/main';
import { GlobalUtils } from '../../utils/GlobalUtils';
import { Button } from 'devextreme-react/button';
import { IAccountConfiguration } from '../../model/config/account';
import { confirm } from 'devextreme/ui/dialog';

type SettingsState = {
    settings: IConfiguration,
    json: {
        data: string
    }
}

const labelName = { text: "Name" };
const labelEngineType = { text: "Engine" };
const editorEngineType = {
    items: [
        { key: "SipJS", name: "SipJS" },
        { key: "Demo", name: "Demo (emulator)" }
    ],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelUserName = { text: "User name or number" };
const labelDomainName = { text: "Domain name" };
const labelLogin = { text: "Login" };
const labelPassword = { text: "Password" };
const labelServerAddress = { text: "Server address" };
const labelServerPort = { text: "Server port" };
const labelAccountEnabled = { text: "Account enabled" };
const editorAccountEnabled = {
    items: [{ key: false, name: "No" }, { key: true, name: "Yes" }],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelRegister = { text: "Registration enabled" };
const editorYesNo = {
    items: [{ key: false, name: "No" }, { key: true, name: "Yes" }],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelVideo = { text: "Video enabled" };
const labelDisplayName = { text: "Display name" };
const labelRingSound = { text: "Ring sound" };
const editorRingSound = {
    items: [
        { key: "", name: "Silence" },
        { key: "ring", name: "Modern phone" },
        { key: "oldphone", name: "Old phone" },
        { key: "mozart", name: "Mozart" },
        { key: "vivaldi", name: "Vivaldi" },
        { key: "clayderman", name: "Clayderman" },
        { key: "queen", name: "Queen" },
        { key: "scorpions", name: "Scorpions" },
        { key: "nirvana", name: "Nirvana" },
    ],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelAllowMultipleActiveCalls = { text: "Allow multiple active calls (otherwise hold them)" };
const editorEnabledDisabled = {
    items: [{ key: false, name: "Disabled" }, { key: true, name: "Enabled" }],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelIncomingCallLimit = { text: "Incoming calls limit (0 - unlimited)" };
const labelAutoAnswer = { text: "Auto answer incoming calls" };
const labelAutoAnswerTimeout = { text: "Auto answer timeout, seconds" };
const labelCodecsAudio = { text: "Audio codecs (ex.: PCMA,G722)" };
const labelCodecsVideo = { text: "Video codecs (ex.: VP8,H264)" };
const labelDisableRTX = { text: "Disable red/rtx for video" };
const labelIceTimeout = { text: "ICE timeout, milliseconds" };
const labelLogLevel = { text: "Log level" };
const editorLogLevel = {
    items: [
        { key: "error", name: "Errors only" },
        { key: "debug", name: "Debug" }
    ],
    valueExpr: 'key',
    displayExpr: 'name'
};
const labelTheme = { text: "Theme" };
const editorTheme = {
    items: [{ key: "dark", name: "Dark" }, { key: "light", name: "Light" }],
    valueExpr: 'key',
    displayExpr: 'name'
};
const editorDialpad = { text: "Dialpad" };
const labelHidden = { visible: false };
const editorCallsHistory = { text: "Calls history" };
const editorJSON = {
    autoResizeEnabled: true,
    stylingMode: "outlined"
};

class SettingsPopup extends React.Component<ui.SettingsProps, SettingsState>{
    constructor(props: ui.SettingsProps) {
        super(props);

        this.state = {
            settings: GlobalUtils.clone(props.data.settings),
            json: {
                data: JSON.stringify(props.data.settings, null, 2)
            }
        }

        this.settings = this.state.settings;

        this.buttonOptionsSave.onClick = this.buttonOptionsSave.onClick.bind(this);
        this.buttonOptionsCancel.onClick = this.buttonOptionsCancel.onClick.bind(this);
        this.buttonOptionsDeleteAccount = this.buttonOptionsDeleteAccount.bind(this);
        
        this.renderTab = this.renderTab.bind(this);
        this.valueChanged = this.valueChanged.bind(this);
        this.optionChanged = this.optionChanged.bind(this);
        this.jsonValueChanged = this.jsonValueChanged.bind(this);
    }

    settings: IConfiguration;

    renderAccounts() {
        var that = this;
        return <div className="paddingTopBig fullSize">
            <ScrollView>
                {this.state.settings.accounts.map(account_ =>
                    <div key={account_.id} className="gridItem">
                        <Form
                            formData={account_}
                            onFieldDataChanged={this.valueChanged}
                            colCount={3}
                        >
                            <SimpleItem
                                dataField="name"
                                label={labelName}
                            />
                            <SimpleItem
                                dataField="engineType"
                                editorType="dxSelectBox"
                                label={labelEngineType}
                                editorOptions={editorEngineType}
                            />
                            <SimpleItem
                                dataField="userName"
                                label={labelUserName}
                            />
                            <SimpleItem
                                dataField="domainName"
                                label={labelDomainName}
                            />
                            <SimpleItem
                                dataField="login"
                                label={labelLogin}
                            />
                            <SimpleItem
                                dataField="password"
                                label={labelPassword}
                            />
                            <SimpleItem
                                dataField="serverAddress"
                                label={labelServerAddress}
                            />
                            <SimpleItem
                                dataField="serverPort"
                                editorType="dxNumberBox"
                                label={labelServerPort}
                            />
                            <SimpleItem
                                dataField="enabled"
                                editorType="dxSelectBox"
                                label={labelAccountEnabled}
                                editorOptions={editorAccountEnabled}
                            />
                            <SimpleItem
                                dataField="register"
                                editorType="dxSelectBox"
                                label={labelRegister}
                                editorOptions={editorYesNo}
                            />
                            <SimpleItem
                                dataField="video"
                                editorType="dxSelectBox"
                                label={labelVideo}
                                editorOptions={editorYesNo}
                            />
                            {
                                <ButtonItem
                                    buttonOptions={this.buttonOptionsDeleteAccount(account_)}
                                />}
                        </Form>
                    </div>)
                }
                <div>
                    <Button
                        width="100%"
                        type="success"
                        text="Add account"
                        focusStateEnabled={false}
                        onClick={function () {
                            var account = new IAccountConfiguration();
                            account.name = "Account " + (that.state.settings.accounts.length + 1);
                            that.state.settings.accounts.push(account);
                            that.setState(that.state);
                        }}
                    />
                </div>
            </ScrollView>
        </div>
    }

    _buttonOptionsDeleteAccount: any = {};

    private buttonOptionsDeleteAccount(account_: IAccountConfiguration): any {
        if (this._buttonOptionsDeleteAccount[account_.id]) {
            return this._buttonOptionsDeleteAccount[account_.id];
        }
        else {
            var that = this;
            var result: any = {
                text: "Delete",
                type: "danger",
                width: "100%",
                onClick: function () {
                    that.deleteAccount(account_);
                }
            };
            this._buttonOptionsDeleteAccount[account_.id] = result;
            return result;
        }
    }

    private deleteAccount(account_: IAccountConfiguration) {
        var that = this;
        confirm("Delete account " + account_.name + "?", "Confirmation").then((result_) => {
            if (result_) {
                that.state.settings.accounts.splice(that.state.settings.accounts.indexOf(account_), 1);
                that.setState(that.state);
            }
        });
    }


    _changed: boolean = false;
    valueChanged() {
        this._changed = true;
        this.settings = this.state.settings;
    }

    updateJSON() {
        if (this._changed) {
            this._changed = false;
            var that = this;
            setTimeout(function () {
                that.setState({
                    ...that.state,
                    json: {
                        data: JSON.stringify(that.state.settings, null, 2)
                    }
                });
            }, 0);
        }
    }

    _jsonChanged: boolean = false;
    updateFromJSON() {
        if (this._jsonChanged) {
            this._jsonChanged = false;
            var that = this;
            setTimeout(function () {
                that.setState({
                    ...that.state,
                    settings: that.settings
                });
            }, 0);
        }
    }

    renderOptions() {
        return <div className="paddingTopBig fullSize">
            <Form
                formData={this.state.settings.options}
                height={"100%"}
                scrollingEnabled={true}
                onFieldDataChanged={this.valueChanged}
            >
                <GroupItem caption="General" colCount={2} >
                    <SimpleItem
                        dataField="displayName"
                        label={labelDisplayName}
                    />
                    <SimpleItem
                        dataField="ringSound"
                        editorType="dxSelectBox"
                        label={labelRingSound}
                        editorOptions={editorRingSound}
                    />
                    <SimpleItem
                        dataField="allowMultipleActiveCalls"
                        editorType="dxSelectBox"
                        label={labelAllowMultipleActiveCalls}
                        editorOptions={editorEnabledDisabled}
                    />
                    <SimpleItem
                        dataField="incomingCallLimit"
                        editorType="dxNumberBox"
                        label={labelIncomingCallLimit}
                    />
                </GroupItem>
                <GroupItem caption="Auto answer calls" colCount={2} >
                    <SimpleItem
                        dataField="autoAnswer"
                        editorType="dxSelectBox"
                        label={labelAutoAnswer}
                        editorOptions={editorEnabledDisabled}
                    />
                    <SimpleItem
                        dataField="autoAnswerTimeout"
                        editorType="dxNumberBox"
                        label={labelAutoAnswerTimeout}
                    />
                </GroupItem>
                <GroupItem caption="Media preferences" colCount={2}>
                    <SimpleItem
                        dataField="codecsAudio"
                        label={labelCodecsAudio}
                    />
                    <SimpleItem
                        dataField="codecsVideo"
                        label={labelCodecsVideo}
                    />
                    <SimpleItem
                        dataField="codecsVideoDisableRTX"
                        editorType="dxSelectBox"
                        label={labelDisableRTX}
                        editorOptions={editorYesNo}
                    />
                    <SimpleItem
                        dataField="iceTimeout"
                        editorType="dxNumberBox"
                        label={labelIceTimeout}
                    />
                </GroupItem>
                <GroupItem caption="Service" colCount={2}>
                    <SimpleItem
                        dataField="logLevel"
                        editorType="dxSelectBox"
                        label={labelLogLevel}
                        editorOptions={editorLogLevel}
                    />
                </GroupItem>
            </Form>
        </div>
    }

    renderDesign() {
        return <div className="paddingTopBig fullSize">
            <Form
                formData={this.state.settings.design}
                height={"100%"}
                scrollingEnabled={true}
                onFieldDataChanged={this.valueChanged}
            >
                <SimpleItem
                    dataField="theme"
                    editorType="dxSelectBox"
                    label={labelTheme}
                    editorOptions={editorTheme}
                />
                <GroupItem caption="Show components" >
                    <SimpleItem
                        dataField="showDialpad"
                        editorType="dxCheckBox"
                        editorOptions={editorDialpad}
                        label={labelHidden} />
                    <SimpleItem
                        dataField="showHistory"
                        editorType="dxCheckBox"
                        editorOptions={editorCallsHistory}
                        label={labelHidden} />
                </GroupItem>
            </Form>
        </div>
    }

    optionChanged(args_: any) {
        if (args_.name === "selectedIndex") {
            if (args_.value === 3)
                this.updateJSON();
            else
                this.updateFromJSON();
        }
    }

    jsonValueChanged() {
        var that = this;
        try {
            var parsed = JSON.parse(that.state.json.data);
            that.settings = parsed;
            that._jsonChanged = true;
            //that.setState({ ...that.state, settings: parsed });
        }
        catch (exception: any) {
            GlobalUtils.notifyError(exception.message);
        }
    }

    renderJSON() {
        return <Form
            formData={this.state.json}
            height={"100%"}
            scrollingEnabled={true}
            onFieldDataChanged={this.jsonValueChanged}
        >
            <SimpleItem
                dataField="data"
                editorType="dxTextArea"
                editorOptions={editorJSON}
                label={labelHidden}
            >
            </SimpleItem>
        </Form>
    }

    renderTab(tabProps: any) {
        if (tabProps.data === "Accounts")
            return this.renderAccounts();
        else if (tabProps.data === "Options")
            return this.renderOptions();
        else if (tabProps.data === "Design")
            return this.renderDesign();
        else if (tabProps.data === "JSON")
            return this.renderJSON();
        else
            return <></>;
    }

    save() {
        var that = this;
        setTimeout(function () {
            that.props.delegates.save(that.settings)
        }, 0);
    }

    private readonly buttonOptionsSave = {
        text: 'Save',
        onClick: this.save
    };

    cancel() {
        var that = this;
        setTimeout(function () {
            that.props.delegates.cancel();
        }, 0);
    }

    private readonly buttonOptionsCancel = {
        text: 'Cancel',
        onClick: this.cancel
    };

    render() {
        const { isLarge } = getScreenSize();

        var tabs = [
            "Accounts",
            "Design",
            "Options",
            "JSON"
        ];

        return (
            <Popup
                className={!isLarge ? "padding": ""}
                visible={true}
                onHiding={this.cancel}
                showTitle={false}
                title="Settings"
                maxWidth={750}
                height={"80%"}
                fullScreen={isLarge ? false : true}
            >
                <TabPanel
                    dataSource={tabs}
                    animationEnabled={false}
                    itemComponent={this.renderTab}
                    height={"100%"}
                    onOptionChanged={this.optionChanged}
                />
                <ToolbarItem
                    key="save"
                    options={this.buttonOptionsSave}
                    widget="dxButton"
                    location="after"
                    toolbar="bottom"
                />
                <ToolbarItem
                    key="cancel"
                    options={this.buttonOptionsCancel}
                    widget="dxButton"
                    location="after"
                    toolbar="bottom"
                />
            </Popup>
        );
    }
}

export default SettingsPopup;
