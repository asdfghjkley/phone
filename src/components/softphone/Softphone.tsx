import React from 'react';
import { Screen, Row, Column, Button, TextInput, Text } from '../elements'
import Dialpad from './Dialpad';
import { ActiveCall } from './ActiveCall'
import { RecentCalls } from './RecentCalls'
import { SoftphoneProps } from '../../model/ui/softphone';
import { ICurrentCall } from '../../model/ui/currentcalls';
import { ECallState } from '../../model/core/call';

const isActive = (call: ICurrentCall) => 
    call.state === ECallState.Dialing || 
    call.state === ECallState.Ringing || 
    call.state === ECallState.Established 

const Softphone = ({ data, delegates }: SoftphoneProps) => {
    let [started, setStarted] = React.useState(false)
    let [showPhone, togglePhone] = React.useState(false)
    let [showHistory, toggleHistory] = React.useState(false)

    let activeCall = data.calls.list.find(isActive)

    let renderToggles = () => {
        return (
            <Row justifyContent='space-between' padding='12px'>
                <Button.Toggle 
                    onClick={() => {
                        if (activeCall?.canUnmute) {
                            delegates.calls.unmute(activeCall)
                        } else if (activeCall?.canMute) {
                            delegates.calls.mute(activeCall)
                        }
                    }}
                    disabled={!activeCall} on={activeCall?.canUnmute}
                    background='lightred' color='red' icon='Voice'
                />
                <Button.Toggle 
                    onClick={() => activeCall?.canTransferTo && delegates.calls.transferTo(activeCall)}
                    disabled={!activeCall} on={activeCall?.canConfirmTransfer}
                    background='lightyellow' color='yellow' icon='Swap'
                />
                <Button.Toggle 
                    onClick={() => {
                        if (activeCall?.canUnhold) {
                            delegates.calls.unhold(activeCall)
                        } else if (activeCall?.canHold) {
                            delegates.calls.hold(activeCall)
                        }
                    }}
                    disabled={!activeCall} on={activeCall?.canUnhold}
                    background='lightblue' color='blue' icon='Lock'
                />
                <Button.Toggle 
                    onClick={() => delegates.accounts.settingsClick()}
                    background='lightgreen' color='green' icon='Setting'
                />
                <Button.Toggle 
                    onClick={() => toggleHistory(!showHistory)}
                    background='lightviolet' color='violet' icon='TimeCircle'
                />
            </Row>
        )
    }

    let renderPhone = () => (
        <Column background='white' border='solid 1px #74889B' width='279px' onClick={() => showHistory && toggleHistory(false)}>
            {activeCall && <ActiveCall data={activeCall} delegates={delegates.calls}/>}
            <Column alignItems='stretch'>
                <Row justifyContent='space-between' padding='24px 24px 6px 24px' background='#F2F5F9'>
                    <Text fontSize='16px' color='#74889B'>Звонок</Text>
                    <Button.TextLink onClick={() => togglePhone(false)}>свернуть</Button.TextLink>
                </Row>
                <Row justifyContent='center' padding='8px 0' position='relative'>
                    <TextInput 
                        value={data.destination.number}
                        onChange={delegates.destination.numberChanged}
                        onEnterKey={delegates.destination.yesClick}
                    />
                    <RecentCalls 
                        data={data.history} 
                        delegates={delegates.history} 
                        isOpen={showHistory}
                        toggleOpen={toggleHistory}
                    />
                </Row>
                <Button.Conference/>
                <Dialpad data={data.dialpad} delegates={delegates.dialpad} />
                <Row justifyContent='space-between' padding='4px 12px'>
                    <Button.Call 
                        disabled={!(!activeCall || activeCall?.canAnswer)} 
                        onClick={() => activeCall ? activeCall?.canAnswer && delegates.calls.answer(activeCall) : delegates.destination.yesClick()}
                    />
                    <Button.Cancel 
                        disabled={!activeCall}
                        onClick={() => activeCall?.canDrop && delegates.calls.drop(activeCall)}
                    />
                </Row>
                {renderToggles()}
            </Column>
        </Column>
    )

    let open = () => {
        if (!started) {
            delegates.start()
            setStarted(true)
        }
        togglePhone(true)
    }

    return (
        <Screen right='16px' bottom='16px'>
            {showPhone ? renderPhone() : <Button.Phone onClick={open}/>}
        </Screen>
    )
}

export default Softphone;