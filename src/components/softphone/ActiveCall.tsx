import React from 'react';
import { Row, Column, Text } from '../elements'
import * as ui from '../../model/ui/currentcalls';

const pad = (s: string) => s.length < 2 ? '00'.substr(0, s.length) + s : s

const format = (ms: number) => {
    let d = new Date(ms)
    d = new Date(d.getTime() + d.getTimezoneOffset() * 60000)
    let [hh, mm, ss] = [d.getHours(), d.getMinutes(), d.getSeconds()].map((n) => pad(n.toString()))
    return `${hh} : ${mm} : ${ss}`
}

const duration = (data: ui.ICurrentCall) => {
    let t = data.duration >= 0 ? data.duration * 1000
        : (new Date().getTime() - (data.stateTime ?? data.timestamp))

    return format(t)
}

export const ActiveCall = ({ data, delegates }: ui.CurrentCallProps) => {
    return <>
        <Column background='#1E2A33' color='white' padding='8px' height='126px' justifyContent='center'>
            <Text color='white' fontSize='20px'>Активный звонок</Text>
            <Text color='#BBBFC2' fontSize='12px'>
                {data.canAnswer ? '(входящий)' : <br/>}
            </Text>
            <Row flex='1'/>
            <Text color='#74889B' fontSize='16px'>
                {data.number}&nbsp;
                {data.canUnhold && <>(<Text display='inline' color='white' fontWeight='500'>удержание</Text>)</>}
            </Text>
            <Text color={data.canUnhold ? '#F5222D' : '#52C41A'} fontSize='20px'>{duration(data)}</Text>
            <div id={`divVideo_${data.id}`} style={{height:0}}/>
        </Column>
    </>
}
