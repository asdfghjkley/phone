import React from 'react';
import { Row, Column, Text, Button } from '../elements'
import * as ui from '../../model/ui/dialpad';

const Dialpad = ({ delegates }: ui.DialpadProps) => {

    let renderDigit = (digit: string, i: number) => (
        <Button key={i} 
            onClick={() => { delegates?.digitClick?.(digit) }}
            flex='1' border='1px solid #D9D9D9' padding='8px 16px' margin='8px' borderRadius='3px'
        >
            <Text fontSize='18px' fontWeight='bold' color='#1E2A33'>{digit}</Text>
        </Button>
    )

    return (
        <Column alignItems='stretch' padding='4px'>
            <Row justifyContent='stretch'>
                {['1', '2', '3'].map(renderDigit)}
            </Row>
            <Row justifyContent='stretch'>
                {['4', '5', '6'].map(renderDigit)}
            </Row>
            <Row justifyContent='stretch'>
                {['7', '8', '9'].map(renderDigit)}
            </Row>
            <Row justifyContent='stretch'>
                {['*', '0', '#'].map(renderDigit)}
            </Row>
        </Column>
    )
}

export default Dialpad;