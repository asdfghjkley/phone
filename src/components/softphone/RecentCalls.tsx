import React from 'react'
import { Column, Icon, Button, Divider, Text, Row } from '../elements'
import { CallsHistoryProps, IHistoryCallData } from '../../model/ui/callshistory'

export const RecentCalls = ({ data, delegates, isOpen, toggleOpen }: CallsHistoryProps) => {
    let renderCall = (call: IHistoryCallData, i: number) => (
        <Column key={i} padding='2px' onClick={() => delegates.callDblClick(call)}>
            <Text color='#BBBFC2' fontSize='12px'>{new Date(call.timestamp).toLocaleString()}</Text>
            <Text color='#1E2A33' fontSize='16px'>{call.number}</Text>
        </Column>
    )

    return (        
        <Column position='absolute' right='20px' top='20px' left='20px' alignItems='end'>
            <Button onClick={() => toggleOpen(!isOpen)}>
                <Icon color='#1890FF' name={isOpen ? 'ChevronUp' : 'ChevronDown'}/>
            </Button>
            {isOpen && (
                <Column position='absolute' padding='8px' background='white' width='100%'>
                    <Divider background='#BBBFC2'/>
                    {data.list.map(renderCall)}
                </Column>
            )}
        </Column>
    )
}
