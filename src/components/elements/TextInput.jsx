import React from 'react'
import { Block } from './Blocks.jsx'

export const TextInput = (props) => {
    let { onEnterKey, onChange, ...rest } = props

    return (
        <Block 
            component='input' type='text'
            onChange={(e) => { onChange?.(e.target.value) }}
            onKeyDown={(e) => { e.key === 'Enter' && onEnterKey?.() }}
            border='1px solid #D9D9D9' color='#1E2A33' padding='4px 8px' width='255px' height='50px' borderRadius='3px' fontFamily='Roboto' fontSize='16px' fontWeight='500'
            {...rest}
        />
    )
}
