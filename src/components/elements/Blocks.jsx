import React from 'react'
import 'typeface-roboto'

export const Styled = React.memo(
    ({ 
        display,
        flexDirection,
        flexWrap,
        flex,
        justifyContent,
        justifySelf,
        alignItems,
        alignSelf,
        position,
        top,
        right,
        bottom,
        left,
        width,
        height,
        margin,
        marginTop,
        marginRight,
        marginBottom,
        marginLeft,
        padding,
        paddingTop,
        paddingRight,
        paddingBottom,
        paddingLeft,
        border,
        borderRadius,
        boxShadow,
        overflowX,
        overflowY,
        lineHeight,
        whiteSpace,
        text,
        textAlign,
        fontFamily,
        fontSize,
        fontWeight,
        fontStyle,
        textDecoration,
        color, 
        background,
        backgroundColor,
        opacity,
        cursor,
        pointerEvents,
        children,
        style,
        component:Component = 'div',
        ...props
    }) => {
        style = {
            ...style,
            display,
            flexDirection,
            flexWrap,
            flex,
            justifyContent,
            justifySelf,
            alignItems,
            alignSelf,
            position,
            top,
            right,
            bottom,
            left,
            width,
            height,
            margin,
            marginTop,
            marginRight,
            marginBottom,
            marginLeft,
            padding,
            paddingTop,
            paddingRight,
            paddingBottom,
            paddingLeft,
            border,
            borderRadius,
            boxShadow,
            overflowX,
            overflowY,
            lineHeight,
            whiteSpace,
            textAlign,
            text,
            fontFamily,
            fontSize,
            fontWeight,
            fontStyle,
            textDecoration,
            color, 
            background,
            backgroundColor,
            opacity,
            cursor,
            pointerEvents,
        }

        for (let key in style) {
            if (style[key] === undefined) delete style[key]
        }

        return (
            <Component style={style} {...props}>
                {children}
            </Component>
        )
    }
)

export const Block = (props) => <Styled {...props}/>

export const Screen = (props) => <Styled position='fixed' {...props}/>

export const Column = (props) => <Styled display='flex' flexDirection='column' {...props}/>

export const Row = (props) => <Styled display='flex' flexDirection='row' {...props}/>

export const Text = (props) => <Styled fontFamily={`'Roboto', sans-serif`} color='#13263d' {...props}/>

export const Divider = (props) => <Styled display='block' width='100%' height='1px' backgroundColor='#dddde1'/>

export const Image = (props) => <Styled component='img' {...props}/>

export const Link = (props) => <Row component='a' href='/' textDecoration='none' color='inherit' {...props}/>

export const Round = ({ size, component: C = Styled, ...props }) => <C display='flex' width={size} height={size} borderRadius='50%' alignItems='center' justifyContent='center' {...props}/>
