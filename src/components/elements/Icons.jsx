import React from 'react'
import { Iconly } from 'react-iconly'

export const Icon = (props) => <Iconly set='light' {...props}/>
