import React from 'react'
import { Row, Round, Text } from './Blocks.jsx'
import { Icon } from './Icons'

export const Button = (props) => {
    let [isHover, setHover] = React.useState()

    let { component: C = Row, ...rest } = props

    return (
        <C 
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            opacity={isHover ? 0.64 : 1} 
            cursor='pointer' 
            justifyContent='center' alignItems='center' 
            {...rest}
        />
    )
}

Button.Phone = (props) => (
    <Button component={Round} size='88px' background='#52C41A' color='white'{...props}>
        <Icon name='Calling'/>
    </Button>
)

Button.Call = ({ disabled = false, ...props }) => (
    <Button background={disabled ? '#D9D9D9' : '#52C41A'} width='120px' height='40px' borderRadius='3px' color='white' {...props}>
        Звонок
    </Button>
)

Button.Cancel = ({ disabled = false, ...props }) => (
    <Button background={disabled? '#D9D9D9' : '#F5222D'} width='120px' height='40px' borderRadius='3px' color='white' {...props}>
        Отбой
    </Button>
)

Button.Conference = (props) => (
    <Button color='black' border='solid 1px #D9D9D9' padding='4px 8px' margin='4px 12px' {...props}>
        <Icon name='People' color='#200E32'/>
        <Text color='#74889B' fontSize='11px'>Конференция</Text>
    </Button>
)

Button.Toggle = (props) => {
    let { disabled, on, color, background, icon, ...rest } = props
    return (
        <Button 
            background={on ? background : 'white'} 
            border={`solid 1px ${on ? color : '#D9D9D9'}`} 
            color={on ? color : '#74889B'}
            opacity={disabled ? 0.64 : 1}
            padding='4px' borderRadius='2px'
            {...rest}
        >
            <Icon name={icon}/>
        </Button>
    )
}

Button.TextLink = ({ children, ...props }) => (
    <Button {...props}>
        <Text color='#1890FF'>{children}</Text>
    </Button>
)