import { IAccount } from "../../model/core/account";
import { ECallDirection, ECallState } from "../../model/core/call";
import { CallBase } from "../base/CallBase";

export class CallDemo extends CallBase {
    constructor(account_: IAccount) {
        super(account_);

        this._number = "";
        this._name = "";
        this._timestamp = new Date().getTime();
        this._direction = ECallDirection.Unknown;
        this._state = ECallState.Unknown;

        this._hasVideo = account_.config.video;
    }

    dispose() {
    }

    make(number_: string, name_: string) {
        super.make(number_, name_);
        var that = this;

        setTimeout(function () {
            if (that.state === ECallState.Dialing) {
                that.state = ECallState.Established;
                setTimeout(function () {
                    if (that.state === ECallState.Established) {
                        that.state = ECallState.Terminated;
                    }
                }, 10000);
            }
        }, 3000);
    }

    incoming(number_: string) {
        var that = this;

        this._number = number_;
        this._direction = ECallDirection.In;
        this.state = ECallState.Ringing;

        setTimeout(function () {
            if (that.state === ECallState.Ringing) {
                that.state = ECallState.Terminated;
            }
        }, 15000);
    }

    answer() {
        var that = this;
        if (this.state === ECallState.Ringing) {
            super.answer();
            this.state = ECallState.Established;
            setTimeout(function () {
                if (that.state === ECallState.Established) {
                    that.state = ECallState.Terminated;
                }
            }, 10000);
        }
    }

    drop() {
        if (this.canDrop()) {
            this._dropSent = true;
            this.state = ECallState.Terminated;
        }
    }

    hold() {
        if (this.canHold())
            this.held = true;
    }

    unhold() {
        if (this.canUnHold())
            this.held = false;
    }

    mute() {
        if (this.canMute())
            this.muted = true;
    }

    unmute() {
        if (this.canUnMute())
            this.muted = false;
    }
}