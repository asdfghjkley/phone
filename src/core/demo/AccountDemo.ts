import { EAccountState, IAccountDelegates } from "../../model/core/account";
import { CallDemo } from "./CallDemo";
import { AccountBase } from "../base/AccountBase";
import { IAccountConfiguration } from "../../model/config/account";

export class AccountDemo extends AccountBase {
    constructor(config_: IAccountConfiguration, delegates_: IAccountDelegates) {
        super(config_, delegates_);

        this.init();
    }
    startAccount() {
        super.startAccount();
        var that = this;
        this.state = EAccountState.Ready;
        this._timer = setInterval(function () {
            var call = new CallDemo(that);
            call.incoming("6411010");
            that.callCreated(call);
        }, 10000);
    }

    stopAccount() {
        super.stopAccount();
        if (this._timer) {
            clearInterval(this._timer);
            this._timer = null;
        }
    }

    makeCall(number_: string, name_: string) { 
        var call = new CallDemo(this);
        call.make(number_, name_);
        this.callCreated(call);
    }

    _timer: NodeJS.Timer | null = null;
}