import { IAccountConfiguration } from "../../model/config/account";
import { EAccountState, IAccount } from "../../model/core/account";
import { ECallState, ICall } from "../../model/core/call";
import { GlobalUtils } from "../../utils/GlobalUtils";

export class AccountBase extends IAccount {
    dispose() {
        this.stopAccount();
    }

    configure(config_: IAccountConfiguration) {
        this._config = GlobalUtils.clone(config_);

        if (!GlobalUtils.isNullOrEmpty(this._config.login) && GlobalUtils.isNullOrEmpty(this._config.userName))
            this._config.userName = this._config.login;
        if (!GlobalUtils.isNullOrEmpty(this._config.userName) && GlobalUtils.isNullOrEmpty(this._config.login))
            this._config.login = this._config.userName;
        if (!GlobalUtils.isNullOrEmpty(this._config.login) && GlobalUtils.isNullOrEmpty(this._config.password))
            this._config.password = this._config.login;

        if (!GlobalUtils.isNullOrEmpty(this._config.serverAddress) && GlobalUtils.isNullOrEmpty(this._config.domainName))
            this._config.domainName = this._config.serverAddress;
        if (!GlobalUtils.isNullOrEmpty(this._config.domainName) && GlobalUtils.isNullOrEmpty(this._config.serverAddress))
            this._config.serverAddress = this._config.domainName;

        this.applyConfig();
    }

    startAccount() {
        GlobalUtils.logDebug("AccountBase#" + this._internalCounter + ".startAccount name=" + this._config.name);
    }

    stopAccount() {
        GlobalUtils.logDebug("AccountBase#" + this._internalCounter + ".stopAccount name=" + this._config.name);
    }

    applyConfig() {
        if (this._config.enabled) {
            this.startAccount();
        }
        else {
            this.stopAccount();
            this._state = EAccountState.Disabled;
        }
    }

    changed() {
        if (this._delegates.accountChanged)
            this._delegates.accountChanged(this);
    }

    callCreated(call_: ICall) {
        this._calls.push(call_);
        if (this._delegates.callCreated)
            this._delegates.callCreated(this, call_);
    }

    callDeleted(call_: ICall) {
        if (this._delegates.callDeleted)
            this._delegates.callDeleted(this, call_);
        var index = this._calls.indexOf(call_);
        if (index >= -1)
            this._calls.splice(index, 1);
        call_.dispose();
    }

    callChanged(call_: ICall) {
        var that = this;
        this._calls.forEach(item_ => {
            if (that._delegates.callChanged)
                that._delegates.callChanged(that, item_);
        });
        /*
        if (this._delegates.callChanged)
            this._delegates.callChanged(this, call_);
            */
        if (call_.state === ECallState.Terminated)
            this.callDeleted(call_);
    }

    getActiveCallsCount(butCallID_?: string) {
        var result = 0;
        this.calls.forEach(call_ => {
            if (call_.state === ECallState.Dialing || call_.state === ECallState.Ringing || call_.state === ECallState.Established) {
                if (butCallID_ && call_.id === butCallID_)
                    return;
                result++;
            }
        })
        return result;
    }
}