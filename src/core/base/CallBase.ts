import { IAccount } from "../../model/core/account";
import { ECallDirection, ECallState, ICall } from "../../model/core/call";
import { GlobalUtils } from "../../utils/GlobalUtils";
import { PlayerUtils } from "../../utils/PlayerUtils";
import { VideoUtils, IVideo } from "../../utils/VideoUtils";
import { AccountManager } from "../engine/AccountManager";
import { Storage } from "../engine/Storage";

export class CallBase extends ICall {
    constructor(account_: IAccount) {
        super();
        this._idaccount = account_.id;
        this._account = account_;
        this._id = GlobalUtils.genUuid();

        this.initializeVideos();

        this.setLimitTimer(120); // 2 минуты
    }

    _account: IAccount;
    _answerSent: boolean = false;
    _dropSent: boolean = false;
    _replaced: boolean = false;
    _intercom: boolean = false;

    dispose() {
        try {
            if (this._videosTimer) {
                clearInterval(this._videosTimer);
                this._videosTimer = undefined;
            }
            this.setLimitTimer(0);
            setTimeout(() => {
                this.playStop();
                GlobalUtils.forceGarbageCollector();
            }, 2000);

            if (this._videos?.cookie) {
                VideoUtils.freeVideos(this._videos.cookie);
                this._videos = undefined;
            }
        }
        finally {
            super.dispose();
        }
    }

    setLimitTimer(interval_: number) {
        if (this._limitTimer) {
            clearTimeout(this._limitTimer);
            this._limitTimer = undefined;
        }
        if (interval_) {
            this._limitTimer = setTimeout(() => {
                GlobalUtils.logError("Time limit: drop call");
                this.drop();
            }, interval_*1000);
        }

    }

    _videos?: IVideo;
    _videosTimer?: any;
    _limitTimer?: any;
    initializeVideos() {
        this._videosTimer = setInterval(() => {
            /*
            if (counter++ > 20) {
                if (this._videosTimer) {
                    clearInterval(this._videosTimer);
                    this._videosTimer = undefined;
                }
                GlobalUtils.logError("CallBase.initializeVideos failed");
                return;
            }
            */
            if (!this._videos) {
                var div = document.getElementById("divVideo_" + this.id);
                if (div) {
                    this._videos = VideoUtils.getVideos();
                    div.appendChild(this._videos.remote);
                    div.appendChild(this._videos.local);
                    if (this._videosTimer) {
                        clearInterval(this._videosTimer);
                        this._videosTimer = undefined;
                    }
                }
                else {
                    GlobalUtils.logDebug("CallBase.initializeVideos iteration failed");
                }
            }
        }, 100);
    }

    _playCookie?: string;
    playStart(sound_: string, loop_: boolean) {
        GlobalUtils.logDebug("CallBase.playStart: " + sound_);
        this.playStop();
        this._playCookie = PlayerUtils.playStart(sound_, loop_);
    }

    playStop() {
        GlobalUtils.logDebug("CallBase.playStop")
        if (this._playCookie) {
            PlayerUtils.playStop(this._playCookie);
            this._playCookie = undefined;
        }
    }

    playRing() {
        GlobalUtils.logDebug("CallBase.playRing " + Storage.Instance.configuration.options.ringSound)
        if (GlobalUtils.isNullOrEmpty(Storage.Instance.configuration.options.ringSound)) {
            return;
        }

        var ringSound = Storage.Instance.configuration.options.ringSound;

        if (AccountManager.Instance) {
            var count = AccountManager.Instance.getEstablishedCallsCount(this.id);
            if (count > 0) {
                this.playStart("reminder", true);
                return;
            }
        }
        
        this.playStart(ringSound, true);
    }

    make(number_: string, name_: string) {
        super.make(number_, name_);
        this._number = number_;
        this._name = name_;
        this._direction = ECallDirection.Out;
        this.state = ECallState.Dialing;
    }

    setState(state_: ECallState) {
        super.setState(state_);
        var that = this;

        if (this._state === state_)
            return;

        if (state_ === ECallState.Ringing && !this._intercom) {
            if (Storage.Instance.configuration.options.autoAnswer) {
                if (Storage.Instance.configuration.options.autoAnswerTimeout <= 0) {
                    setTimeout(function () {
                        if (that.state === ECallState.Ringing) {
                            that.answer();
                        }
                    }, 0);
                }
                else {
                    setTimeout(function () {
                        if (that.state === ECallState.Ringing) {
                            that.answer();
                        }
                    }, Storage.Instance.configuration.options.autoAnswerTimeout * 1000);
                    this.playRing();
                }
            }
            else {
                this.playRing();
            }
        }
        else if (state_ === ECallState.Established) {
            this.setLimitTimer(60 * 125); // 2 часа 5 минут 
            this.playStop();
        }
        else if (state_ === ECallState.Terminated) {
            var playBusy = this.state !== ECallState.Ghost && this.state !== ECallState.Ringing && !this._dropSent && !this._replaced;
            GlobalUtils.logDebug("CallBase.terminated: playBusy=" + playBusy + " state=" + this.state + " dropSent=" + this._dropSent + " replaced=" + this._replaced);
            if (playBusy)
                this.playStart("busy", false);
            else
                this.playStop();
        }

        this._state = state_;
        this.changed();
    }

    changed() {
        if (this._account)
            this._account.callChanged(this);
    }

    canDrop() {
        return this.state === ECallState.Dialing || this.state === ECallState.Ringing || this.state === ECallState.Established || this.state === ECallState.Ghost;
    }

    answer() {
        GlobalUtils.logDebug("CallBase.answer sphdebug A");
        this._answerSent = true;
        GlobalUtils.logDebug("CallBase.answer sphdebug B");
    }

    canAnswer() {
        return !this._answerSent && this.state === ECallState.Ringing;
    }

    canHold() {
        return this.state === ECallState.Established && !this._held;
    }

    canUnHold() {
        return this.state === ECallState.Established && this._held;
    }

    canMute() {
        return (this.state === ECallState.Established || this.state === ECallState.Dialing || this.state === ECallState.Ringing) && !this._muted;
    }

    canUnMute() {
        return this._muted;
    }

    canSendDTMF() {
        return this.state === ECallState.Established && !this._held;
    }

}