import { AccountBase } from "../base/AccountBase";
import { EAccountState, IAccountDelegates } from "../../model/core/account";
import { IAccountConfiguration } from "../../model/config/account";
import { CallSipJS } from "./CallSipJS";
import { Storage } from "../engine/Storage";
import { GlobalUtils } from "../../utils/GlobalUtils";
import { Invitation, Referral, Registerer,  UserAgent, UserAgentOptions, UserAgentState } from "sip.js";
import { IncomingResponse } from "sip.js/lib/core";

var sipjs = (document as any)["era_lib_sipjs"];
//console.log("CallSipJS module init")
//console.log(sipjs)

export class AccountSipJS extends AccountBase {
    constructor(config_: IAccountConfiguration, delegates_: IAccountDelegates) {
        super(config_, delegates_);

        this.stateChanged = this.stateChanged.bind(this);
        this.transportStateChanged = this.transportStateChanged.bind(this);
        this.registeredChanged = this.registeredChanged.bind(this);
        this.onInvite = this.onInvite.bind(this);
        this.onTransportMessage = this.onTransportMessage.bind(this);
        this.doRegister = this.doRegister.bind(this);

        this.init();
    }

    _userAgent: UserAgent | null = null;
    _registerer: Registerer | null = null;

    startAccount() {
        GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".startAccount");
        this.stopAccount();
        super.startAccount();
        var that = this;

        var strUri = "sip:" + this._config.userName + "@" + this._config.domainName;

        var uri = sipjs.UserAgent.makeURI(strUri);
        if (!uri) {
            GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".startAccount: " + strUri + " is not a valid uri");
            this._errorMessage = strUri + " is not a valid uri";
            this.state = EAccountState.Error;
            return;
        }

        var userAgentOptions: UserAgentOptions = {
            uri,
            displayName: Storage.Instance.configuration.options.displayName,
            authorizationUsername: that._config.login,
            authorizationPassword: that._config.password,
            transportOptions: {
                server: "wss://" + this._config.serverAddress + ":" + this._config.serverPort
            },
            logLevel: Storage.Instance.configuration.options.logLevel === "debug" ? "debug" : "error",
            sessionDescriptionHandlerFactoryOptions: {
                iceGatheringTimeout: Storage.Instance.configuration.options.iceTimeout
            },
            delegate: {
                onInvite: this.onInvite,
            }
        }

        this.state = EAccountState.Initializing;

        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".startAccount: create user agent");

        that._userAgent = new sipjs.UserAgent(userAgentOptions) as UserAgent;
        that._userAgent.stateChange.addListener(that.stateChanged);
        that._userAgent.transport.stateChange.addListener(that.transportStateChanged);

        var savedHandler = that._userAgent.transport.onMessage;
        that._userAgent.transport.onMessage = (message_: string) => {
            that.onTransportMessage(message_);
            if (savedHandler)
                savedHandler(message_);
        };

        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".startAccount: start user agent");
        that._userAgent.start().then(() => {
            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".startAccount: user agent started");
        }, () => {
            GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".startAccount: user agent start failed");
            that._errorMessage = "Starting user agent failed";
            that.state = EAccountState.Error;
        }).catch((reason_: string) => {
            GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".startAccount.failed");
            GlobalUtils.logError(reason_);
            that._errorMessage = "Starting user agent failed";
            that.state = EAccountState.Error;
        });
    }

    onTransportMessage(message_: string) {
        if (this._userAgent) {
            try {
                var parsed = sipjs.Core.Parser.parseMessage(message_, this._userAgent.getLogger("sip.Parser"));
                if (parsed) {
                    var callID = parsed.callId;

                    var call = this._calls.find(call_ => call_._nativeID === callID);
                    if (call instanceof CallSipJS) {
                        (call as CallSipJS).onTransportMessage(parsed);
                    }
                }
            }
            catch (exception: any) {
                GlobalUtils.logError("AccountSipJS#" + this._internalCounter + ".onTransportMessage.failed");
                GlobalUtils.logError(exception.message);
            }
        }
    }

    stopAccount() {
        var that = this;
        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".stopAccount");
        super.stopAccount();

        if (this._registerer) {
            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".stopAccount: disposing registerer")

            this._registerer.stateChange.removeListener(that.registeredChanged)

            this._registerer.dispose().catch((reason_) => {
                GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".registerer.dispose failed");
                GlobalUtils.logError(reason_);
            });
            this._registerer = null;
        }

        if (this._userAgent) {
            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".stopAccount: disposing userAgent")

            this._userAgent.stateChange.removeListener(that.stateChanged);
            this._userAgent.transport.stateChange.removeListener(that.transportStateChanged);
    
            this._userAgent.stop().catch((reason_) => {
                GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".userAgent.dispose failed");
                GlobalUtils.logError(reason_);
            
            });
            this._userAgent = null;
        }
    }

    stateChanged(state_: UserAgentState) {
        GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".stateChanged: " + state_)
    }

    transportStateChanged(state_: string) {
        var that = this;
        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".transportStateChanged: " + state_)

        if (that._userAgent === null) {
            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".transportStateChanged ignored: already disposed");
            return;
        }

        switch (state_) {
            case "Connected":
                if (that._config.register) {
                    that.register();
                }
                else {
                    that.state = EAccountState.Ready;
                }
                break;
            case "Disconnected":
                that._errorMessage = "Socket disconnected";
                that.state = EAccountState.Error;

                if (this._registerer !== null) {
                    this._registerer.dispose().catch((reason_) => {
                        GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".transportStateChanged.Disconnected.registerer.dispose failed");
                        GlobalUtils.logError(reason_);
                    });
                    this._registerer = null;
                }
  
                setTimeout(() => {
                    if (that._userAgent) {
                        that.state = EAccountState.Initializing;
                        that._userAgent.transport.connect().catch((reason_) => {
                            GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".transportStateChanged.Disconnected.connect failed");
                            GlobalUtils.logError(reason_);
                        });
                    }
                }, 1000);
                break;
        }
    }

    register() {
        var that = this;
        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".register")
        if (that._config.register && that._userAgent) {
            if (!that._registerer) {
                GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".register: create registerer")
                that._registerer = new sipjs.Registerer(that._userAgent, {
                    expires: Storage.Instance.configuration.options.registrationExpires
                }) as Registerer;
                that._registerer.stateChange.addListener(that.registeredChanged);
            }
            that.doRegister();
        }
    }

    doRegister() {
        var that = this;
        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".doRegister")
        if (that._registerer) {
            that._registerer.register({
                requestDelegate: {
                    onReject(response: IncomingResponse) {
                        that._errorMessage = (response.message.statusCode + " " + response.message.reasonPhrase) ?? "Registration failed";
                    }
                }
            }).catch((reason_) => {
                GlobalUtils.logError("AccountSipJS#" + that._internalCounter + ".doRegister.register failed");
                GlobalUtils.logError(reason_);
            });
        }

        setTimeout(function () {
            if (that._registerer !== null) {
                if (that._registerer.state !== "Registered") {
                    GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".doRegister.timerTick - firing unregistered");
                    that.registeredChanged("Unregistered");
                }
            }
        }, 5000);
    }

    _registerTimer: NodeJS.Timeout | null = null;

    registeredChanged(state_: string) {
        var that = this;
        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".registeredChanged: " + state_)

        if (that._registerer === null) {
            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".registeredChanged ignored: already disposed");
            return;
        }

        switch (state_) {
            case "Registered":
                this.state = EAccountState.Ready;
                break;
            case "Unregistered":
                if (this._config.register) {
                    this.state = EAccountState.Error;
                    if (!this._registerTimer) {
                        GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".registeredChanged.startTimer");
                        this._registerTimer = setTimeout(() => {
                            that._registerTimer = null;
                            GlobalUtils.logDebug("AccountSipJS#" + that._internalCounter + ".registeredChanged.timerTick - register");
                            that.register();
                        }, 5000);
                    }
                }
                break;
        }
    }

    makeCall(number_: string, name_: string) {
        GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".makeCall: number=" + number_);
        var call = new CallSipJS(this);
        this.callCreated(call);
        call.make(number_, name_);
    }

    onInvite(invitation_: Invitation) {
        GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".onInvite activeCallsCount=" + this.getActiveCallsCount() + " limit=" + Storage.Instance.configuration.options.incomingCallLimit);

        if (Storage.Instance.configuration.options.incomingCallLimit) {
            if (this.getActiveCallsCount() >= Storage.Instance.configuration.options.incomingCallLimit) {
                GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".onInvite: drop by incoming limit");
                setTimeout(function () {
                    invitation_.reject({ statusCode: 486 });
                }, 0);
                return;
            }
        }

        var call = new CallSipJS(this);
        this.callCreated(call);
        call.onInvite(invitation_);
    }

    onRefer(call_: CallSipJS, referral_: Referral) {
        GlobalUtils.logDebug("AccountSipJS#" + this._internalCounter + ".onRefer");
        var call = new CallSipJS(this);
        this.callCreated(call);
        return call.makeInviteByRefer(referral_);
    }

}