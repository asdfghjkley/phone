import { ECallDirection, ECallState } from "../../model/core/call";
import { GlobalUtils } from "../../utils/GlobalUtils";
import { CallBase } from "../base/CallBase";
import { AccountSipJS } from "./AccountSipJS";
import { Storage } from "../engine/Storage";
import { Invitation, Inviter, InviterInviteOptions, Referral, Session, SessionInviteOptions } from "sip.js";
import { IncomingMessage, IncomingResponseMessage } from "sip.js/lib/core";
import { AccountManager } from "../engine/AccountManager";

var sipjs = (document as any)["era_lib_sipjs"];
//console.log("CallSipJS module init")
//console.log(sipjs)

export class CallSipJS extends CallBase {
    constructor(account_: AccountSipJS) {
        super(account_);
        this._account = account_;

        this._number = "";
        this._name = "";
        this._timestamp = new Date().getTime();
        this._direction = ECallDirection.Unknown;
        this._state = ECallState.Unknown;

        this._inviter = null;
        this._invitation = null;

        this._hasVideo = account_.config.video;

        this.sessionStateChanged = this.sessionStateChanged.bind(this);
        this.sdpModifier = this.sdpModifier.bind(this);
        this.onRefer = this.onRefer.bind(this);

    }

    _account: AccountSipJS;
    _inviter: Inviter | null;
    _invitation: Invitation | null;


    getSession(): Session | null {
        return this._inviter ?? this._invitation;
    }

    _fromTag: string = "";
    _toTag: string = "";

    dispose() {
        try {
            if (this._t1) {
                clearInterval(this._t1);
                this._t1 = undefined;
            }
            if (this._t2) {
                clearInterval(this._t2);
                this._t2 = undefined;
            }
        }
        finally {
            super.dispose();
        }
    }

    getMediaOptions(): InviterInviteOptions {
        return {
            sessionDescriptionHandlerOptions: {
                constraints: {
                    audio: true,
                    video: this._account.config.video
                },
            }
        };
    }

    assignSessionDelegates(session_: Session) {
        session_.stateChange.addListener(this.sessionStateChanged);
        if (session_.delegate)
            session_.delegate.onRefer = this.onRefer;
        else {
            session_.delegate = {
                onRefer: this.onRefer
            };
        }
    }

    make(number_: string, name_: string) {
        GlobalUtils.logDebug("CallSipJS.make number=" + number_);
        super.make(number_, name_);

        var strUri = "sip:" + number_ + "@" + this._account.config.domainName;

        var uri = sipjs.UserAgent.makeURI(strUri);
        if (!uri) {
            GlobalUtils.logDebug("CallSipJS.make :" + strUri + " is not a valid uri");
            GlobalUtils.notifyError(strUri + " is not a valid uri");
            this.state = ECallState.Terminated;
            return;
        }

        if (this._account._userAgent) {
            var inviter = new sipjs.Inviter(this._account._userAgent, uri, { earlyMedia: true }) as Inviter;
            this.assignSessionDelegates(inviter);
            this._inviter = inviter;

            inviter.sessionDescriptionHandlerModifiers = [this.sdpModifier];
            this._nativeID = inviter.request.callId;

            inviter.invite(this.getMediaOptions()).catch((error_) => {
                GlobalUtils.logError("CallSipJS.make.invite failed")
                GlobalUtils.logError(error_)
                this.state = ECallState.Terminated;
            });
        }
        else {
            GlobalUtils.logError("CallSipJS.make: account does not have UserAgent");
            this.state = ECallState.Terminated;
        }
    }

    makeInviteByRefer(referral_: Referral) {
        GlobalUtils.logDebug("CallSipJS.makeInviteByRefer")
        if (referral_.request) {
            var referTo = referral_.request.getHeader("Refer-To");
            if (referTo) {
                var uri = sipjs.Core.Grammar.nameAddrHeaderParse(referTo);
                if (uri && uri.uri && uri.uri.user)
                    this._number = uri.uri.user;
            }
        }

        if (GlobalUtils.isNullOrEmpty(this._number))
            this._number = "Transfer";
        this._direction = ECallDirection.Out;
        this.state = ECallState.Dialing;

        if (this._account._userAgent) {
            var inviter = referral_.makeInviter({});
            this.assignSessionDelegates(inviter);
            this._inviter = inviter;

            inviter.sessionDescriptionHandlerModifiers = [this.sdpModifier];
            this._nativeID = inviter.request.callId;

            inviter.invite(this.getMediaOptions()).catch((error_) => {
                GlobalUtils.logError("CallSipJS.makeInviteByRefer.invite failed")
                GlobalUtils.logError(error_)
                this.state = ECallState.Terminated;
            });
        }
        else {
            GlobalUtils.logError("CallSipJS.makeInviteByRefer: account does not have UserAgent");
            this.state = ECallState.Terminated;
        }
    }

    onInvite(invitation_: Invitation) {
        GlobalUtils.logDebug("CallSipJS.onInvite")

        var that = this;

        if (invitation_.request.data.indexOf("intercom=true") >= 0 ||
            invitation_.request.data.indexOf("answer-after=0") >= 0) {
            GlobalUtils.logDebug("CallSipJS.onInvite: intercom detected");
            this._intercom = true;
        }

        this._nativeID = invitation_.request.callId;
        this._number = invitation_.remoteIdentity.uri.user ?? "";
        this._name = invitation_.remoteIdentity.displayName ?? "";
        this._direction = ECallDirection.In;
        this.state = ECallState.Ringing;

        invitation_.sessionDescriptionHandlerModifiers = [this.sdpModifier];
        this.assignSessionDelegates(invitation_);
        this._invitation = invitation_;

        if (this._intercom) {
            setTimeout(function () {
                if (that.canAnswer()) {
                    AccountManager?.Instance?.holdAllOthers(that);
                    that.answer();
                }
            }, 0);
        }
        var replaces = invitation_.request.getHeader("Replaces");
        if (replaces) {
            GlobalUtils.logDebug("CallSipJS.onInvite: replaces detected " + replaces);

            var items = replaces.split(";");
            if (items.length >= 3) {
                var callID = items[0];
                var fromTag = items[1];
                if (fromTag.startsWith("from-tag="))
                    fromTag = fromTag.substr(9);
                var toTag = items[2];
                if (toTag.startsWith("to-tag="))
                    toTag = toTag.substr(7);
                
                var oldCall: CallSipJS | null = null;
                this._account.calls.forEach(call_ => {
                    if (call_ instanceof CallSipJS) {
                        var callEx = call_ as CallSipJS;
                        GlobalUtils.logDebug("Checking " + callEx._nativeID + "=" + callID);
                        GlobalUtils.logDebug("Checking " + callEx._fromTag + "=" + fromTag);
                        GlobalUtils.logDebug("Checking " + callEx._toTag + "=" + toTag);
                        if (callEx._nativeID === callID/* &&
                            callEx._fromTag === fromTag &&
                            callEx._toTag === toTag*/) {
                            oldCall = callEx;
                        }
                    }
                    if (oldCall) {
                        GlobalUtils.logDebug("CallSipJS.onInvite Replaced call found");
                        var oldCallState = oldCall.state;
                        oldCall.drop();
                        if (oldCallState === ECallState.Established) {
                            setTimeout(function () {
                                if (that.canAnswer()) {
                                    that.answer();
                                }
                            }, 0);
                        }
                        else {
                            this.playRing();
                        }
                    }
                });
            }
        }
    }

    sessionStateChanged(state_: string) {
        switch (state_) {
            case "Establishing":
                break;
            case "Established":
                this.state = ECallState.Established;
                this.updateMedia();
                break;
            case "Terminating":
                this.state = ECallState.Terminating;
                this.cleanupMedia();
                break;
            case "Terminated":
                this.state = ECallState.Terminated;
                this.cleanupMedia();
                break;
        }
    }
    
    onRefer(referral_: Referral) {
        var that = this;
        GlobalUtils.logDebug("CallSipJS.onRefer")
        referral_
            .accept()
            .then(() => {
                that._account.onRefer(that, referral_);
                this._replaced = true;
            })
            .catch((error_) => {
                GlobalUtils.logError("CallSipJS.onRefer.accept failed")
                GlobalUtils.logError(error_);
            });
    }

    _remoteSsrc: string = "";

    sdpModifier(description: RTCSessionDescriptionInit): Promise<RTCSessionDescriptionInit> {
        if (!description.sdp)
            return Promise.resolve(description);

        var lines = description.sdp.split("\n");

        if (description.sdp.indexOf("127.0.0.1") < 0) {
            // remote sdp - check ssrc changed and reassign media

            var ssrcLine = lines.find(line_ => line_.startsWith("a=ssrc:"));
            if (ssrcLine) {
                var ssrc = ssrcLine.split(" ")[0].split(":")[1];
                if (!GlobalUtils.isNullOrEmpty(this._remoteSsrc) && this._remoteSsrc !== ssrc) {
                    GlobalUtils.logDebug("Remote SSRC changed detected (" + this._remoteSsrc + " -> " + ssrc + "): updateMedia")
                    this.updateMedia();
                }
                this._remoteSsrc = ssrc;
            }
            return Promise.resolve(description);
        }

        //GlobalUtils.logDebug("===================== SDP processing (codecs) =========================");
        //GlobalUtils.logDebug(description.sdp);
        //GlobalUtils.logDebug("=======================================================================");

        var linesNew: string[] = [];

        var disableRTX = Storage.Instance.configuration.options.codecsVideoDisableRTX;

        var audioCodecs: string[] = (Storage.Instance.configuration.options.codecsAudio ?? "").split(",")
            .filter(item_ => !GlobalUtils.isNullOrEmpty((item_ ?? "").trim())); // ["PCMU"];
        var videoCodecs: string[] = (Storage.Instance.configuration.options.codecsVideo ?? "").split(",")
            .filter(item_ => !GlobalUtils.isNullOrEmpty((item_ ?? "").trim())); // ["VP9"];
        
        if (audioCodecs.indexOf("*") >= 0)
            audioCodecs = [];
        if (videoCodecs.indexOf("*") >= 0)
            videoCodecs = [];

        var audioPayloads: string[] = [];
        var videoPayloads: string[] = [];

        var codecs: string[] = [];
        var payloads: string[] = [];
        var payload = "";

        var line = "";

        for (var i = 0; i < lines.length; i++) {
            line = lines[i];

            if (disableRTX) {
                if (line.indexOf("rtx/") >= 0) {
                    i++;
                    continue;
                }
                if (line.indexOf("red/") >= 0) {
                    continue;
                }
            }

            if (line.indexOf("m=audio") === 0) {
                payload = "";
                codecs = audioCodecs;
                payloads = audioPayloads;
                GlobalUtils.logDebug("Audio codecs: " + codecs.join(", "))
            }
            else if (line.indexOf("m=video") === 0) {
                payload = "";
                codecs = videoCodecs;
                payloads = videoPayloads;
                GlobalUtils.logDebug("Video codecs: " + codecs.join(", "))
            }

            if (codecs.length && line.indexOf("a=") === 0) {
                if (line.indexOf("a=rtpmap") === 0) {
                    payload = line.split(":")[1].split(" ")[0];
                    for (var c = 0; c < codecs.length; c++){
                        if (line.indexOf(" " + codecs[c] + "/") >= 0) {
                            payloads.push(payload);
                        }
                    }
                }
                else {
                    if (line.indexOf(":" + payload + " ") < 0) {
                        payload = "";
                    }
                }

                if (payload !== "") {
                    var found = false;
                    for (var d = 0; d < payloads.length; d++) {
                        if (line.indexOf(":" + payloads[d] + " ") >= 0)
                            found = true;
                    }
                    if (!found)
                        continue;
                }
            }
            linesNew.push(line);
        }

        for (var k = 0; k < linesNew.length; k++) {
            line = linesNew[k];

            payloads = [];

            if (line.indexOf("m=audio") === 0) {
                GlobalUtils.logDebug("Audio SDP original: " + line);
                if (audioPayloads.length)
                    payloads = audioPayloads;
            }
            else if (line.indexOf("m=video") === 0) {
                GlobalUtils.logDebug("Video SDP original: " + line);
                if (videoPayloads.length)
                    payloads = videoPayloads;
            }

            if (payloads.length) {
                var items = line.trim().split(" ");
                var newItems: string[] = [];
                for (var j = 0; j < items.length; j++) {
                    var item = items[j];
                    if (j <= 2 || payloads.indexOf(item) >= 0)
                        newItems.push(item);
                }
                line = newItems.join(" ");
                GlobalUtils.logDebug("SDP modified: " + line);
                linesNew[k] = line;
            }
        }

        description.sdp = linesNew.join("\n");

        //GlobalUtils.logDebug("===================== SDP changed =========================");
        //GlobalUtils.logDebug(description.sdp);
        //GlobalUtils.logDebug("===========================================================");

        return Promise.resolve(description);
    }

    holdModifier(description: RTCSessionDescriptionInit): Promise<RTCSessionDescriptionInit> {
        if (!description.sdp || description.sdp.indexOf("127.0.0.1") < 0) {
            return Promise.resolve(description);
        }

        //GlobalUtils.logDebug("===================== SDP processing (hold) ===========================");
        //GlobalUtils.logDebug(description.sdp);
        //GlobalUtils.logDebug("=======================================================================");

        if (!description.sdp) {
            return Promise.resolve(description);
        }
        if (!(/a=(sendrecv|sendonly|recvonly|inactive)/).test(description.sdp)) {
            description.sdp = description.sdp.replace(/(m=[^\r]*\r\n)/g, "$1a=sendonly\r\n");
        }
        else {
            description.sdp = description.sdp.replace(/a=sendrecv\r\n/g, "a=sendonly\r\n");
            description.sdp = description.sdp.replace(/a=recvonly\r\n/g, "a=inactive\r\n");
        }

        //GlobalUtils.logDebug("===================== SDP changed =========================");
        //GlobalUtils.logDebug(description.sdp);
        //GlobalUtils.logDebug("===========================================================");

        return Promise.resolve(description);
    }

    assignStream(stream_: any, element_: any, play_: boolean) {
        if (play_) {
            element_.autoplay = true; // Safari does not allow calling .play() from a non user action
        }
        else {
            element_.autoplay = false; // Safari does not allow calling .play() from a non user action
        }
        var ms = stream_;//new MediaStream(stream_);
        element_.srcObject = ms;
        element_.load();

        //GlobalUtils.logDebug("CallSipJS.assignStream: stream assigned to element")
        //GlobalUtils.logDebug(stream_)
        //GlobalUtils.logDebug(ms)
        //GlobalUtils.logDebug(element_)

        if (play_) {
            element_.play().catch((error_: any) => {
                GlobalUtils.logError("CallSipJS.assignStream: failed to play media");
                GlobalUtils.logError(error_);
            });
        }
  
        // If a track is added, load and restart playback of media.
        stream_.onaddtrack = () => {
            GlobalUtils.logDebug("CallSipJS.assignStream.onaddtrack");

            element_.load(); // Safari does not work otheriwse
            if (play_) {
                element_.play().catch((error_: any) => {
                    GlobalUtils.logError("CallSipJS.assignStream: failed to play media on add track");
                    GlobalUtils.logError(error_);
                });
            }
        };
  
        // If a track is removed, load and restart playback of media.
        stream_.onremovetrack = () => {
            GlobalUtils.logDebug("CallSipJS.assignStream.onremovetrack");

            element_.load(); // Safari does not work otheriwse
            if (play_) {
                element_.play().catch((error_: any) => {
                    GlobalUtils.logError("CallSipJS.assignStream: failed to play media on remove track");
                    GlobalUtils.logError(error_);
                });
            }
        };
    }

    updateMedia() {
        GlobalUtils.logDebug("CallSipJS.updateMedia");
        var that = this;
        setTimeout(() => {
            if (that.state === ECallState.Established || that.state === ECallState.Dialing) {
                GlobalUtils.logDebug("CallSipJS.updateMedia: timer tick - setup media streams");
                that.setupMedia();
                that.setMute(that.muted);
            }
        }, 100);

    }

    private _t1?: any;
    private _t2?: any;
    setupMedia() {
        GlobalUtils.logDebug("CallSipJS.setupMedia");

        if (this._t1) {
            clearInterval(this._t1);
            this._t1 = undefined;
        }
        if (this._t2) {
            clearInterval(this._t2);
            this._t2 = undefined;
        }

        var that = this;
        var session = this.getSession();
        if (!session)
            return;
        
        const sessionDescriptionHandler: any = session.sessionDescriptionHandler;
        
        const tryRemote = () => {
            if (that._videos) {
                if (sessionDescriptionHandler) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: assign remote stream")
                    that.assignStream(sessionDescriptionHandler.remoteMediaStream, that._videos.remote, true)
                    return true;
                }
                else {
                    GlobalUtils.logError("CallSipJS.setupMedia: sessionDescriptionHandler is null");
                    return false;
                }
            }
            else {
                GlobalUtils.logDebug("CallSipJS.setupMedia: waiting for remote video element...");
                return false;
            }

            /*
            var remoteVideo = document.getElementById("remoteVideo_" + that.id);
            if (remoteVideo) {
                if (sessionDescriptionHandler) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: assign remote stream")
                    that.assignStream(sessionDescriptionHandler.remoteMediaStream, remoteVideo)
                    return true;
                }
                else {
                    GlobalUtils.logError("CallSipJS.setupMedia: sessionDescriptionHandler is null");
                    return false;
                }
            }
            else {
                GlobalUtils.logDebug("CallSipJS.setupMedia: waiting for remote video element...");
                return false;
            }
            */
        }
        var c1 = 0;
        this._t1 = setInterval(() => {
            if (this._t1) {
                if (tryRemote()) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: remote video assigned");
                    clearInterval(this._t1);
                    this._t1 = undefined;
                }
                if (c1++ > 100) {
                    GlobalUtils.logError("CallSipJS.setupMedia: failed to assign remote video");
                    clearInterval(this._t1);
                    this._t1 = undefined;
                }
            }
        }, 100);

        const tryLocal = () => {
            if (that._videos) {
                if (sessionDescriptionHandler) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: assign local stream")
                    that.assignStream(sessionDescriptionHandler.localMediaStream, that._videos.local, false)
                    return true;
                }
                else {
                    GlobalUtils.logError("CallSipJS.setupMedia: sessionDescriptionHandler is null");
                    return false;
                }
            }
            else {
                GlobalUtils.logDebug("CallSipJS.setupMedia: waiting for local video element...");
                return false;
            }

            /*
            var localVideo = document.getElementById("localVideo_" + that.id);
            if (localVideo) {
                if (sessionDescriptionHandler) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: assign local stream")
                    that.assignStream(sessionDescriptionHandler.localMediaStream, localVideo)
                    return true;
                }
                else {
                    GlobalUtils.logError("CallSipJS.setupMedia: sessionDescriptionHandler is null");
                    return false;
                }
            }
            else {
                GlobalUtils.logDebug("CallSipJS.setupMedia: waiting for local video element...");
                return false;
            }
            */
        }
        var c2 = 0;
        this._t2 = setInterval(() => {
            if (this._t2) {
                if (tryLocal()) {
                    GlobalUtils.logDebug("CallSipJS.setupMedia: local video assigned");
                    clearInterval(this._t2);
                    this._t2 = undefined;
                }
                if (c2++ > 100) {
                    GlobalUtils.logError("CallSipJS.setupMedia: failed to assign local video");
                    clearInterval(this._t2);
                    this._t2 = undefined;
                }
            }
        }, 100);
    }

    cleanupMedia() {
        GlobalUtils.logDebug("CallSipJS.cleanupMedia");
    }


    answer() {
        GlobalUtils.logDebug("CallSipJS.answer");
        if (this.canAnswer()) {
            GlobalUtils.logDebug("CallSipJS.answer sphdebug A");
            super.answer();
            GlobalUtils.logDebug("CallSipJS.answer sphdebug B");
            if (this._invitation) {
                GlobalUtils.logDebug("CallSipJS.answer sphdebug C");
                GlobalUtils.logDebug("CallSipJS.answer.invitation.accept");
                this._invitation.accept(this.getMediaOptions()).then(() => {
                    GlobalUtils.logDebug("CallSipJS.answer sphdebug E");
                }).catch((reason_) => {
                    GlobalUtils.logDebug("CallSipJS.answer sphdebug D");
                    GlobalUtils.logError("CallSipJS.answer failed");
                    GlobalUtils.logError(reason_);
                });
                GlobalUtils.logDebug("CallSipJS.answer sphdebug F");
            }
        }
        else {
            GlobalUtils.logDebug("CallSipJS.answer sphdebug !canAnswer");
        }
    }

    drop() {
        GlobalUtils.logDebug("CallSipJS.drop");
        var that = this;
        if (this.canDrop()) {
            this._dropSent = true;
            if (this._inviter) {
                this._inviter.dispose().catch((reason_) => {
                    GlobalUtils.logError("CallSipJS.drop.inviter.dispose failed");
                    GlobalUtils.logError(reason_);
                });
                this._inviter = null;
            }
            if (this._invitation) {
                this._invitation.dispose().catch((reason_) => {
                    GlobalUtils.logError("CallSipJS.drop.invitation.dispose failed");
                    GlobalUtils.logError(reason_);
                });
                this._invitation = null;
            }
            setTimeout(() => {
                if (that.state !== ECallState.Terminated) {
                    GlobalUtils.logError("CallSipJS.drop: timeout - force terminated (state is now " + that.state + ")");
                    that.state = ECallState.Terminated;
                }
            }, 1000);
        }
    }

    hold() {
        GlobalUtils.logDebug("CallSipJS.hold");
        if (this.canHold())
            this.setHold(true);
    }

    unhold() {
        GlobalUtils.logDebug("CallSipJS.unhold");
        if (this.canUnHold())
            this.setHold(false);
    }

    setHold(hold_: boolean, transferToNumber_?: string) {
        GlobalUtils.logDebug("CallSipJS.setHold: " + hold_);

        this.playStop();

        var that = this;
        var session = this.getSession();
        if (!session) {
            GlobalUtils.logError("CallSipJS.setHold: session is null");
            return;
        }

        if (this.held === hold_)
            return;
        
        var options: SessionInviteOptions = {
            requestDelegate: {
                onAccept: () => {
                    that.held = hold_;
                    that.setMute(that.muted);
                    if (transferToNumber_) {
                        GlobalUtils.logDebug("CallSipJS.setHold.onAccept - doTransferTo");
                        this.doTransferTo(transferToNumber_);
                    }

                    if (!that.held) {
                        GlobalUtils.logDebug("CallSipJS.unhold success: restore media");
                        that.updateMedia();
                    }
                },
                onReject: () => {
                    GlobalUtils.logDebug("CallSipJS.setHold: re-invite request was rejected");
                    this.drop();
                },
            },
        };
        if (hold_) {
            options.sessionDescriptionHandlerModifiers = [this.sdpModifier, this.holdModifier];
        }
        else {
            options.sessionDescriptionHandlerModifiers = [this.sdpModifier];
        }

        return session.invite(options)
            .then(() => {
            })
            .catch((error_) => {
                GlobalUtils.logError("CallSipJS.setHold: reinvite failed");
                GlobalUtils.logError(error_);
            });
    }

    mute() {
        GlobalUtils.logDebug("CallSipJS.mute");
        if (this.canMute())
            this.setMute(true);
    }

    unmute() {
        GlobalUtils.logDebug("CallSipJS.unmute");
        if (this.canUnMute())
            this.setMute(false);
    }

    setMute(mute_: boolean) {
        GlobalUtils.logDebug("CallSipJS.setMute: " + mute_);
        this.muted = mute_;
        if (this.state === ECallState.Established)
            this.enableSenderTracks(!this.muted && !this.held);
    }

    enableSenderTracks(enabled: boolean) {
        GlobalUtils.logDebug("CallSipJS.enableSenderTracks: " + enabled);
        var session = this.getSession();
        if (!session) {
            GlobalUtils.logError("CallSipJS.enableSenderTracks: session is null");
            return;
        }

        const sessionDescriptionHandler: any = session.sessionDescriptionHandler;
        if (sessionDescriptionHandler) {
            var peerConnection = sessionDescriptionHandler.peerConnection;
            if (peerConnection) {
                GlobalUtils.logDebug("CallSipJS.enableSenderTracks.senders dump");
                peerConnection.getSenders().forEach((sender_: any) => {
                    GlobalUtils.logDebug(sender_);
                    if (sender_.track && sender_.track.kind === "audio") {
                        sender_.track.enabled = enabled;
                    }
                });
                GlobalUtils.logDebug("CallSipJS.enableSenderTracks.receivers dump");
                peerConnection.getReceivers().forEach((receiver_: any) => {
                    GlobalUtils.logDebug(receiver_);
                })
            }
        }
    }

    setReason(reason_: string) {
        if (reason_.startsWith("SIP/2.0 "))
            reason_ = reason_.substr(8);
        
        if (reason_.startsWith("100") || reason_.startsWith("202"))
            return;
        
        this._reason = reason_;
        this.changed();
    }

    onTransportMessage(message_: IncomingMessage) {
        var that = this;

        this._fromTag = message_.from.getParam("tag") ?? "";
        this._toTag = message_.to.getParam("tag") ?? "";
    
        var remoteIdentity = message_.getHeader("Remote-Party-ID")
        if (remoteIdentity) {
            GlobalUtils.logDebug("CallSipJS.onTransportMessage: Remote-Party-ID");
            var uri = sipjs.Core.Grammar.nameAddrHeaderParse(remoteIdentity);
            if (uri) {
                if (uri.uri && uri.uri.user)
                    this.number = uri.uri.user;
                if (uri.displayName)
                    this.name = uri.displayName;
            }
        }

        var response = message_ as IncomingResponseMessage;
        if (response && response.statusCode) {
            this.setReason(response.statusCode + " " + response.reasonPhrase);
            if (response.statusCode) {
                if (response.statusCode === 180) {
                    that.playStart("backring", true);
                }
                if (response.statusCode >= 180 && response.statusCode <= 189) {
                    if (response.statusCode !== 180)
                        this.playStop();
                    if (response.getHeader("Content-Type") === "application/sdp") {
                        that.updateMedia();
                    }
                }
            }
        }
        if (message_.method === "NOTIFY") {
            var event = message_.getHeader("Event");
            if (event) {
                if (event === "talk") {
                    GlobalUtils.logDebug("CallSipJS.onTransportMessage: Event=talk")
                    setTimeout(function () {
                        GlobalUtils.logDebug("CallSipJS.onTransportMessage: Event=talk PROCESSING")

                        if (that.canAnswer()) {
                            that.answer();
                        }
                        else if (that.canUnHold()) {
                            that.unhold();
                        }
                    }, 100);
                }
                else if (event === "hold") {
                    GlobalUtils.logDebug("CallSipJS.onTransportMessage: Event=hold")
                    setTimeout(function () {
                        if (that.canHold()) {
                            that.hold();
                        }
                    }, 0);
                }
                else if (event.startsWith("refer")) {
                    GlobalUtils.logDebug("CallSipJS.onTransportMessage: Event=refer " + message_.body);
                    this.setReason(message_.body);
                    if (message_.body.startsWith("SIP/2.0 2")) {
                        setTimeout(function () {
                            if (that.canDrop()) {
                                that.drop();
                            }
                        }, 0);
                    }
                    var words = message_.body.split(" ");
                    if (words.length >= 2) {
                        var statusCode = words[1];
                        if (statusCode >= "400") {
                            this.state = ECallState.Established;
                            this.playRing();
                        }
                    }
                }

            }
        }
        else if (message_.method === "CANCEL" && this.state === ECallState.Established) {
            GlobalUtils.logDebug("CallSipJS.onTransportMessage: CANCEL received at Established state");
            setTimeout(() => {
                if (this.state === ECallState.Established)
                    this.drop();
            }, 100);
        }
    }

    canTransferTo() {
        return this.state === ECallState.Established;
    }

    canConfirmTransfer() {
        var that = this;
        return this.state === ECallState.Established &&
            this.held &&
            (this._account.calls.find(item => item.id !== that.id && item.state === ECallState.Established && !item.held) !== undefined);
    }

    confirmTransfer() {
        GlobalUtils.logDebug("CallSipJS.confirmTransfer");
        var that = this;
        if (this.state === ECallState.Established && this.held) {
            var target = this._account.calls.find(item => item.id !== that.id && !item.held && item.state === ECallState.Established);
            if (target instanceof CallSipJS) {
                var targetEx = target as CallSipJS;
                var targetSession = targetEx.getSession();
                if (targetSession) {
                    this.doConfirmTransfer(targetSession);
                }
                else {
                    GlobalUtils.logError("CallSipJS.confirmTransfer: target not found");
                }
            }
        }
        else {
            GlobalUtils.logError("CallSipJS.confirmTransfer: invalid state");
        }
    }

    transferTo(number_: string) {
        GlobalUtils.logDebug("CallSipJS.transferTo: number=" + number_);
        if (!this.canTransferTo())
            return;
        if (this.held || !this.canHold())
            this.doTransferTo(number_);
        else {
            this.setHold(true, number_);
        }
    }
        
    doTransferTo(number_: string) {
        GlobalUtils.logDebug("CallSipJS.doTransferTo " + number_);
        var session = this.getSession();
        if (!session) {
            GlobalUtils.logError("CallSipJS.doTransferTo: session is null");
            return;
        }

        var strUri = "sip:" + number_ + "@" + this._account.config.domainName;

        var uri = sipjs.UserAgent.makeURI(strUri);
        if (!uri) {
            GlobalUtils.logError("CallSipJS.doTransferTo: " + strUri + " is not a valid uri");
            GlobalUtils.notifyError(strUri + " is not a valid uri");
            return;
        }

        this.state = ECallState.Ghost;
        session.refer(uri).catch((error_: any) => {
            GlobalUtils.logError("CallSipJS.doTransferTo.refer failed")
            GlobalUtils.logError(error_)
        });
    }

    doConfirmTransfer(target_: Session) {
        GlobalUtils.logDebug("CallSipJS.doConfirmTransfer doConfirmTransfer " + target_.id);
        var session = this.getSession();
        if (!session) {
            GlobalUtils.logError("CallSipJS.doConfirmTransfer: session is null");
            return;
        }

        session.refer(target_).catch((error_: any) => {
            GlobalUtils.logError("CallSipJS.doConfirmTransfer.refer failed")
            GlobalUtils.logError(error_)
        });
    }

    sendDTMF(digit_: string) {
        GlobalUtils.logDebug("CallSipJS.sendDTMF " + digit_);
        var session = this.getSession();
        if (!session) {
            GlobalUtils.logError("CallSipJS.sendDTMF: session is null");
            return;
        }

        const duration = 200;
        const body = {
            contentDisposition: "render",
            contentType: "application/dtmf-relay",
            content: "Signal=" + digit_ + "\r\nDuration=" + duration
        };
        const requestOptions = { body };
        session.info({ requestOptions }).catch((error_: any) => {
            GlobalUtils.logError("CallSipJS.sendDTMF failed")
            GlobalUtils.logError(error_)
        });
    }
}