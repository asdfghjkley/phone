import { IConfiguration } from "../../model/config/main";
import { IAccount, IAccountDelegates } from "../../model/core/account";
import { ECallState, ICall } from "../../model/core/call";
import { IManagerDelegates } from "../../model/core/manager";
import { GlobalUtils } from "../../utils/GlobalUtils";
import { AccountFactory } from "./AccountFactory";
import { Storage } from "./Storage";

export class AccountManager {
    constructor(delegates_: IManagerDelegates) {
        this._delegates = delegates_;
        AccountManager.Instance = this;
    }

    static Instance: AccountManager | null;

    dispose() {
        this._delegates = {
            accountsChanged: () => { },
            accountChanged: (account_: IAccount) => { },
            callCreated: (account_: IAccount, call_: ICall) => { },
            callDeleted: (account_: IAccount, call_: ICall) => { },
            callChanged: (account_: IAccount, call_: ICall) => { }
        }
    }

    _delegates: IManagerDelegates;
    _config: IConfiguration = new IConfiguration();
    _accounts: IAccount[] = [];

    get accounts(): IAccount[] {
        return this._accounts;
    }

    applyConfig(config_: IConfiguration) {
        var that = this;
        this._config = GlobalUtils.clone(config_);

        this._accounts.forEach(account_ => account_.dispose());
        this._accounts = [];

        // create all new accounts

        var delegates: IAccountDelegates = {
            accountChanged: (account_: IAccount) => {
                if (that._delegates.accountChanged)
                    that._delegates.accountChanged(account_);
            },
            callCreated: (account_: IAccount, call_: ICall) => {
                if (that._delegates.callCreated)
                    that._delegates.callCreated(account_, call_);
            },
            callDeleted: (account_: IAccount, call_: ICall) => {
                if (that._delegates.callDeleted)
                    that._delegates.callDeleted(account_, call_);
            },
            callChanged: (account_: IAccount, call_: ICall) => {
                if (that._delegates.callChanged)
                    that._delegates.callChanged(account_, call_);
            },
        }

        this._config.accounts.forEach(account_ => {
            var account = AccountFactory.CreateAccount(account_, delegates);
            if (account)
                this._accounts.push(account);
        });

        // todo: update instead of create-drop

        if (this._delegates && this._delegates.accountsChanged)
            this._delegates.accountsChanged();
    }

    accountChanged(account_: IAccount) {
        if (this._delegates && this._delegates.accountChanged)
            this._delegates.accountChanged(account_);
    }

    makeCall(idAccount_: string, number_: string, name_: string) {
        var account = this._accounts.find(item_ => item_.id === idAccount_);
        if (account) {
            this.holdAll();
            account.makeCall(number_, name_);
        }
        else
            GlobalUtils.notifyError("Account not found");
    }

    callByID(id_: string): ICall | null {
        var result = null;
        this._accounts.forEach(account_ => {
            account_.calls.forEach(call_ => {
                if (call_.id === id_)
                    result = call_;
            })
        })
        return result;
    }

    holdAll() {
        if (Storage.Instance.configuration.options.allowMultipleActiveCalls)
            return;
        GlobalUtils.logDebug("AccountManager.holdAll");
        this._accounts.forEach(account_ => {
            account_.calls.forEach(item_ => {
                if (item_.canHold()) {
                    setTimeout(function () {
                        item_.hold();
                    }, 0);
                }
            })
        })
    }

    holdAllOthers(call_: ICall) {
        if (Storage.Instance.configuration.options.allowMultipleActiveCalls)
            return;
        GlobalUtils.logDebug("AccountManager.holdAllOthers but " + call_.id);
        this._accounts.forEach(account_ => {
            account_.calls.forEach(item_ => {
                if (item_.id !== call_.id && item_._nativeID !== call_._nativeID && item_.canHold()) {
                    setTimeout(function () {
                        item_.hold();
                    }, 0);
                }
            })
        });
    }

    answerCall(idCall_: string) {
        GlobalUtils.logDebug("AccountManager.answerCall sphdebug A");
        var call = this.callByID(idCall_);
        if (call) {
            GlobalUtils.logDebug("AccountManager.answerCall sphdebug B");
            call.answer();
            GlobalUtils.logDebug("AccountManager.answerCall sphdebug C");
            this.holdAllOthers(call);
            GlobalUtils.logDebug("AccountManager.answerCall sphdebug D");
        }
        GlobalUtils.logDebug("AccountManager.answerCall sphdebug E");
    }

    dropCall(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.drop();
    }

    holdCall(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.hold();
    }

    unholdCall(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call) {
            call.unhold();
            this.holdAllOthers(call);
        }
    }

    transferCallTo(idCall_: string, number_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.transferTo(number_);
    }

    confirmTransfer(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.confirmTransfer();
    }

    muteCall(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.mute();
    }

    unmuteCall(idCall_: string) {
        var call = this.callByID(idCall_);
        if (call)
            call.unmute();
    }

    dropAllCalls() {
        var calls: ICall[] = [];
        this._accounts.forEach(account_ => {
            account_.calls.forEach(call_ => {
                calls.push(call_);
            })
        })
        calls.forEach(call_ => call_.drop());
    }

    sendDTMFtoActiveCalls(digit_: string) {
        var calls: ICall[] = [];
        this._accounts.forEach(account_ => {
            account_.calls.forEach(call_ => {
                if (call_.canSendDTMF())
                    calls.push(call_);
            })
        })
        if (calls.length) {
            calls.forEach(call_ => call_.sendDTMF(digit_));
            return true;
        }
        else {
            return false;
        }
    }


    getEstablishedCallsCount(butCallID_?: string) {
        var result = 0;
        this._accounts.forEach(account_ => {
            account_.calls.forEach(call_ => {
                if (call_.state === ECallState.Established) {
                    if (butCallID_ && call_.id === butCallID_)
                        return;
                    result++;
                }
            })
        });
        return result;
    }

}