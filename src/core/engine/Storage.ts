import { EEngineType, IAccountConfiguration } from "../../model/config/account";
import { IConfiguration } from "../../model/config/main";
import { IHistoryCallData } from "../../model/ui/callshistory";
import { GlobalUtils } from "../../utils/GlobalUtils";

export class Storage {
    static Instance = new Storage();

    _configuration: IConfiguration = new IConfiguration();
    get configuration() { return this._configuration; }
    set configuration(value_: IConfiguration) { this._configuration = value_; this.configurationChanged(); }

    _callbackConfigurationChanged: () => void = () => { };

    subscribeToConfiguration(callback_: () => void) {
        this._callbackConfigurationChanged = callback_;
        GlobalUtils.logDebug("subscribeToConfiguration");
    }

    configurationChanged() {
        GlobalUtils.logDebug("configuration changed");
        if (this._callbackConfigurationChanged)
            this._callbackConfigurationChanged();
    }

    history: IHistoryCallData[] = [];

    constructor() {
        this.load();

        var items = document.location.href.split("?");
        if (items.length >= 2) {
            var params = items[1];
            var url = items[0];

            var parameters = params.split("&");
            var accountData: any = {};
            parameters.forEach(parameter_ => {
                var kv = parameter_.split("=");
                if (kv.length === 2)
                    accountData[kv[0]] = kv[1];
            });

            var account = this.configuration.accounts[0];
            if (account === undefined) {
                account = new IAccountConfiguration();
                this.configuration.accounts.push(account);
            }

            if (accountData.login) {
                account.login = accountData.login;
                account.userName = accountData.login;
            }
            if (accountData.pwd)
                account.password = accountData.pwd;
            if (accountData.address)
                account.serverAddress = accountData.address;
            if (accountData.port)
                account.serverPort = accountData.port;
            if (accountData.domain)
                account.domainName = accountData.domain;
            
            if (account.name === "")
                account.name = "Era";
            if (account.engineType === EEngineType.Unknown)
                account.engineType = EEngineType.SipJS;

            this.save();

            if (accountData.pwd)
                accountData.pwd = "***";
            
            console.log("Account parameters override: " + JSON.stringify(accountData));

            window.history.replaceState({}, document.title, url);
        }
    }

    load() {
        var saved = localStorage.eraPhoneConfiguration;
        if (saved && typeof saved === "string") {
            try {
                this.configuration = JSON.parse(saved);
            }
            catch {
            }
        }
        saved = localStorage.eraPhoneHistory;
        if (saved && typeof saved === "string") {
            try {
                this.history = JSON.parse(saved);
            }
            catch {
            }
        }
    }

    save() {
        localStorage.eraPhoneConfiguration = JSON.stringify(this.configuration);
        localStorage.eraPhoneHistory = JSON.stringify(this.history);
    }
}
