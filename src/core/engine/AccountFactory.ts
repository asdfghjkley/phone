import { IAccountConfiguration, EEngineType } from "../../model/config/account";
import { IAccount, IAccountDelegates } from "../../model/core/account";
import { AccountDemo } from "../demo/AccountDemo";
import { AccountError } from "../error/AccountError";
import { AccountSipJS } from "../sipjs/AccountSipJS";

export class AccountFactory {
    static CreateAccount(config_: IAccountConfiguration, delegates_: IAccountDelegates): IAccount | undefined {
        switch (config_.engineType) {
            case EEngineType.Demo:
                return new AccountDemo(config_, delegates_);
            case EEngineType.SipJS:
                return new AccountSipJS(config_, delegates_);
            default:
                return new AccountError(config_, delegates_, "Unknown account.engineType: " + config_.engineType);
        }
    }
}