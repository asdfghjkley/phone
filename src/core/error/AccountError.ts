import { IAccountConfiguration } from "../../model/config/account";
import { EAccountState, IAccountDelegates } from "../../model/core/account";
import { AccountBase } from "../base/AccountBase";

export class AccountError extends AccountBase {
    constructor(config_: IAccountConfiguration, delegates_: IAccountDelegates, errorMessage_: string) {
        super(config_, delegates_);
        this._state = EAccountState.Error;
        this._errorMessage = errorMessage_.toString();
    }

}