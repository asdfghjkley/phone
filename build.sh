#!/bin/bash
set -e
export NODE_OPTIONS="--max-old-space-size=8192"

cd `dirname $0`
rm -rf build
npm ci && npm run build
find build -type f -print0 | xargs -0 chmod -x
rm -rf _build
mv build _build
